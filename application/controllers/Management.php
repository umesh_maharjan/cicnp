<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends CI_Controller {

	function __construct()
  {
    
   parent:: __construct();
   $this->load->helper(array('form','url','html','date'));
   $this->load->model('Management_m');
   $this->load->model('Loan_user_m');
      $this->load->model('Common_m');
   $this->load->library('pagination');
 }

 public function index(){

   if(!$this->session->userdata('login')){
    redirect('Login');

  }

  $getemail=$this->session->userdata('email');
$usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}




  $id=$this->session->userdata('id');



  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);

  $data['count']=$totalloanapplied-$totalviewed;

  $this->load->view('header');
  $this->load->view('menu',$data);


  $data['bod']=$this->Management_m->getbod($id);
  $this->load->view('bod/bodlist',$data);

}


public function getlcm(){

	if(!$this->session->userdata('login')){
		redirect('Login');
	}

  $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
	$id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);

  $data['count']=$totalloanapplied-$totalviewed;

  $this->load->view('header');
  $this->load->view('menu',$data);


  $data['lcm']=$this->Management_m->getlcm($id);
  $this->load->view('lcm/lcmlist',$data);
}

public function addbod_view(){
  if(!$this->session->userdata('login')){
    redirect('Login');

  }
  $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}

  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);

  $data['count']=$totalloanapplied-$totalviewed;
  $this->load->view('header');
  $this->load->view('menu',$data);

  $this->load->view('bod/add_bod');
}


public function addlcm_view(){
  if(!$this->session->userdata('login')){
    redirect('Login');

  }
$usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}

  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);

  $data['count']=$totalloanapplied-$totalviewed;
  $this->load->view('header');
  $this->load->view('menu',$data);

  $this->load->view('lcm/add_lcm');
}


function addbod(){
     if(!$this->session->userdata('login')){
    redirect('Login');

  }
  $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $params['organization_id']=$this->session->userdata('id');

  $params['name']=$this->input->post('name');
  $params['gender']=$this->input->post('gender');
  $params['type']=$this->input->post('type');
  $params['address']=$this->input->post('address');
  $contact=$this->input->post('contact');
  for($i=0;$i<count($contact);$i++){
   $data_c[]= $contact[$i];
 }


 $education=$this->input->post('education');
 for($i=0;$i<count($education);$i++){
   $data_ed[]= $education[$i];
 }
 $params['contact']=implode(",",$data_c);
 $params['edu_qulfy']=implode(",",$data_ed);

 if($params['type']=='BOD'){
   $config['upload_path'] = './uploads/bodphoto';
 }
 else{
  $config['upload_path'] = './uploads/lcm';	
}
$config['allowed_types'] = 'gif|jpg|png';
$config['overwrite']    =  TRUE;

				//$config['encrypt_name'] = true;

$config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
				// $config['max_height']  = '720';   
$config['overwrite']    =  TRUE;
$config['file_name']  = uniqid(rand());	
$this->load->library('upload', $config);
$this->upload->initialize($config);

$type= explode('.',$_FILES['image']['name']);
$type= $type[count($type)-1];

$params['photo'] = $config['file_name'].'.'.$type;


$upload = $this->upload->data();


if(!$this->upload->do_upload('image'))
 
{


 $error = array('error' => $this->upload->display_errors());              
 $this->session->set_flashdata('error', $this->upload->display_errors());  

 if($params['type']=='BOD'){
  redirect('Management/addbod_view');
}
else{
 redirect('Management/addlcm_view');
}
}
else{

  $data=$this->Management_m->insert($params);
  if($data){

    $this->session->set_flashdata('success', 'Successfully added BOD');  
    if($params['type']=='BOD'){
     redirect('management');
   }
   else{
    redirect('management/getlcm');	
  }
}

}
}

function editbod_view(){

  $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->input->get('id');
  $data['bod']=$this->Management_m->getbod_id($id);



  $org=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($org);

  $data['count']=$totalloanapplied-$totalviewed;

  $this->load->view('header');
  $this->load->view('menu',$data);

  $this->load->view('bod/edit_bod',$data);


}

function editlcm_view(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->input->get('id');
  $data['bod']=$this->Management_m->getbod_id($id);


  $org=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($org);

  $data['count']=$totalloanapplied-$totalviewed;
  $this->load->view('header');
  $this->load->view('menu',$data);

  $this->load->view('lcm/edit_lcm',$data);


}

function editbod(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->input->post('id');
  $params['name']=$this->input->post('name');
  $params['gender']=$this->input->post('gender');
  $params['address']=$this->input->post('address');
  $usertype=$this->input->post('type');


  $contact=$this->input->post('contact');
  for($i=0;$i<count($contact);$i++){
   $data_c[]= $contact[$i];
 }


 $education=$this->input->post('education');
 for($i=0;$i<count($education);$i++){
   $data_ed[]= $education[$i];
 }
 $params['contact']=implode(",",$data_c);
 $params['edu_qulfy']=implode(",",$data_ed);



 if($usertype=='BOD'){
   $config['upload_path'] = './uploads/bodphoto';
 }
 else{
  $config['upload_path'] = './uploads/lcm';	
}
$config['allowed_types'] = 'gif|jpg|png';
$config['overwrite']    =  TRUE;

				//$config['encrypt_name'] = true;

$config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
				// $config['max_height']  = '720';   
$config['overwrite']    =  TRUE;
$config['file_name']  = uniqid(rand());	
$this->load->library('upload', $config);
$this->upload->initialize($config);

$type= explode('.',$_FILES['image']['name']);
$type= $type[count($type)-1];

$params['photo'] = $config['file_name'].'.'.$type;


$upload = $this->upload->data();




if(!$this->upload->do_upload('image'))
 
{


  $getphoto=$this->Management_m->getphoto($id);


  foreach($getphoto as $p){
   $photo=$p->photo;
 }
 $params['photo']=$photo;

 $data=$this->Management_m->update($id,$params);

 if($usertype=='BOD'){
 	  $this->session->set_flashdata('success', 'Successfully edited BOD'); 
  redirect('Management');
}
else{
	  $this->session->set_flashdata('success', 'Successfully edited LCM'); 
 redirect('Management/getlcm');
}
}



else{

 $getphoto=$this->Management_m->getphoto($id);
 foreach($getphoto as $p){
   $photo=$p->photo;
 }


 if($usertype=='BOD'){
   unlink ("./uploads/bodphoto/".$photo );
 }
 else{
  unlink ("./uploads/lcm/".$photo );
}


$data=$this->Management_m->update($id,$params);
if($data){

 
  if($usertype=='BOD'){

    $this->session->set_flashdata('success', 'Successfully edited BOD');  
   redirect('Management');
 }
 else{
    $this->session->set_flashdata('success', 'Successfully edited LCM');  
  redirect('Management/getlcm');	
}
}




}}

//notification view 
function notificationview(){
        if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->session->userdata('id');



  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);
  $data['count']=$totalloanapplied-$totalviewed;





  $config["base_url"] = base_url() . "management/notificationview";
  $config['use_page_numbers']  = TRUE;
  $config["per_page"] = 2;
  $config['total_rows']= $totalloanapplied;
  $config['full_tag_open'] = '<ul class="pagination">';
  $config['full_tag_close'] = '</ul>';
  $config['first_link'] = false;
  $config['last_link'] = false;
  $config['first_tag_open'] = '<li>';
  $config['first_tag_close'] = '</li>';
  $config['prev_link'] = '&laquo';
  $config['prev_tag_open'] = '<li class="prev">';
  $config['prev_tag_close'] = '</li>';
  $config['next_link'] = '&raquo';
  $config['next_tag_open'] = '<li>';
  $config['next_tag_close'] = '</li>';
  $config['last_tag_open'] = '<li>';
  $config['last_tag_close'] = '</li>';
  $config['cur_tag_open'] = '<li class="active"><a href="#">';
  $config['cur_tag_close'] = '</a></li>';
  $config['num_tag_open'] = '<li>';
  $config['num_tag_close'] = '</li>';
  $this->pagination->initialize($config);
  if($this->uri->segment(3)){
    $page = ($this->uri->segment(3)-1) ;
  }
  else{
    $page = 0;
  }

// $data["teacher"] = $this->faculty_m->getteacher($config["per_page"],$page);
  $data["links"] = $this->pagination->create_links();

  $this->load->view('header');
  $this->load->view('menu',$data);
  $data['limit']=($config["per_page"] *$page)+1;


  $data['notification']=$this->Loan_user_m->getloanlist($config["per_page"],$page);
  $this->load->view('notification/notificationlist',$data);

}

function notificationdetail(){
         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}

  $id=$this->input->get('id');
  $org_id=$this->session->userdata('id');

//$data['notification']=$this->Loan_user_m->getloanlist();





  $checknotification=$this->Loan_user_m->checknotification($org_id,$id);

  if($checknotification){

  }
  else{
   $item['org_id']=$org_id;
   $item['loan_id']=$id;
   $item['view']='1';
   $updatenotification=$this->Loan_user_m->updatenotification($item);
 }

 $data['notification']=$this->Loan_user_m->getnotificationdetail($id);

 if($data){

   $totalloanapplied=$this->Management_m->getloancount();
   $totalviewed=$this->Management_m->getnotificationcount($org_id);
   $data['count']=$totalloanapplied-$totalviewed;
   $this->load->view('header');
   $this->load->view('menu',$data);
   $this->load->view('notification/notificationdetail',$data);
 }
 

}

 //delete bod

function delete(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $data['id']=$this->input->get('id');
  $data['users']=$this->Management_m->getbod_id($data['id']);



  $this->load->view('header');


  $this->load->view('bod/delete_bod',$data);
}


function confirm_delete(){
  $id=$_POST['id'];
  
  $params=$this->Management_m->getphoto($id);
  foreach($params as $p);{
    unlink ("./uploads/bodphoto/".$p->photo );
  }

  $data=$this->Management_m->deleteuser($id);
  $this->session->set_flashdata('success', 'Delete successfully');
  redirect('Management'); 
    //}
     // else{
  $this->session->set_flashdata('error', ' Server error  Please try again !');
  redirect('Management'); 
  
}


     //delete lcm


function deletelcm(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $data['id']=$this->input->get('id');
  $data['users']=$this->Management_m->getbod_id($data['id']);



  $this->load->view('header');


  $this->load->view('lcm/delete_lcm',$data);
}


function confirm_deletelcm(){
  $id=$_POST['id'];
  
  $params=$this->Management_m->getphoto($id);
  foreach($params as $p);{
    unlink ("./uploads/lcm/".$p->photo );
  }

  $data=$this->Management_m->deleteuser($id);
  $this->session->set_flashdata('success', 'Delete successfully');
  redirect('Management/getlcm'); 
    //}
     // else{
  $this->session->set_flashdata('error', ' Server error  Please try again !');
  redirect('Management/getlcm'); 
  
}


//otm
function omtlist(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);
  $data['count']=$totalloanapplied-$totalviewed;
  $this->load->view('header');
  $this->load->view('menu',$data);


  $data['otm']=$this->Management_m->getotmlist($id);
  $this->load->view('otm/otmlist',$data);

}

function addomtview(){



       if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
$id=$this->session->userdata('id');

$totalloanapplied=$this->Management_m->getloancount();
$totalviewed=$this->Management_m->getnotificationcount($id);
$data['count']=$totalloanapplied-$totalviewed;
$this->load->view('header');
$this->load->view('menu',$data);

$this->load->view('otm/add_omt');

}
function addomt(){
         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $params=array();
  $params['name']=$this->input->post('name');
  $params['position']=$this->input->post('position');
  $params['org_id']=$this->session->userdata('id');
  $data=$this->Management_m->insertomt($params);
  if($data){
   $this->session->set_flashdata('success', 'Successfully add OMT!');
   redirect('Management/omtlist'); 
 }
}

function editomtview(){

         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->input->get('id');

  $org_id=$this->session->userdata('id');
  $data['omt']=$this->Management_m->getomtid($id);

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($org_id);
  $data['count']=$totalloanapplied-$totalviewed;
  $this->load->view('header');
  $this->load->view('menu',$data);
  $this->load->view('otm/edit_omt',$data);

}
function editomt(){
         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->input->post('id');
  $params['name']=$this->input->post('name');
  $params['position']=$this->input->post('position');

  $data=$this->Management_m->updateomt($id,$params);
  if($data){
    $this->session->set_flashdata('success', 'Successfully Edit OMT!');
    redirect('Management/omtlist'); 
  }
  else{
    $this->session->set_flashdata('error', 'Server error Please try again!');
    redirect('Management/editomtview?id='.$id); 
  }
}



function deleteomt(){
        if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $data['id']=$this->input->get('id');
  $data['users']=$this->Management_m->getomtid($data['id']);



  $this->load->view('header');


  $this->load->view('otm/delete_omt',$data);
}


function confirm_deleteomt(){
  $id=$_POST['id'];

  $data=$this->Management_m->deleteomt($id);
  $this->session->set_flashdata('success', 'Delete successfully');
  redirect('Management/omtlist'); 
    //}
     // else{
  $this->session->set_flashdata('error', ' Server error  Please try again !');
  redirect('Management/omtlist'); 
  
}
public function changepasswordorg(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}

  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);
  $data['count']=$totalloanapplied-$totalviewed;
  
  $this->load->view('header');
  $this->load->view('menu',$data);

  $this->load->view('user/changepassword');
}


public function updatepassword(){
         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
  $id=$this->session->userdata('id');
  $oldpassword = $this->input->post('old_password');
  $newpassword=$this->input->post('new_password');
  $confirm_pw = $this->input->post('confirm_password');

  $checkoldpassword=$this->Common_m->getorg($id);

  foreach($checkoldpassword as $p){
    $old_pass=$p->password;
    $salt=$p->salt;
    
  }

  $psw=sha1($oldpassword.$salt.'Db_Project');
  if($psw !=$old_pass){
    $this->session->set_flashdata('error', 'Old Password doesnt match');
    redirect('Management/changepasswordorg');
  }else if($oldpassword==$newpassword){
    $this->session->set_flashdata('error', 'New password same as old password');
    redirect('Management/changepasswordorg');
  }
  else if($newpassword != $confirm_pw){
    $this->session->set_flashdata('error', 'New password and confirm password doesnt match ');
    redirect('Management/changepasswordorg');
  }
  else{
    $data['password']=sha1($newpassword.$salt.'Db_Project');
    $this->Common_m->update($id,$data,'cooperative_list');
    $this->session->set_flashdata('success', 'Password changed Successfully ');
    redirect('Management/changepasswordorg');
  }
}

function editprofile(){
 if(!$this->session->userdata('login')){
  redirect('Login');
}
    $usertype=$this->session->userdata('usertype');
if($usertype!='Admin'){
  redirect('Login');
}
$id=$this->session->userdata('id');


$totalloanapplied=$this->Management_m->getloancount();
$totalviewed=$this->Management_m->getnotificationcount($id);
$data['count']=$totalloanapplied-$totalviewed;



$this->load->view('header');
$this->load->view('menu',$data);
$data['org']=$this->Common_m->getorg($id);
$this->load->view('user/edit_profile',$data);
}

function updateprofile(){
 $id=$this->session->userdata('id');


  $params['name']=$this->input->post('name');
  $params['address']=$this->input->post('address');

  $params['reg_no']=$this->input->post('rege_no');   

//$params['contact_no']=$this->input->post('contact');
  $params['contact_person']=$this->input->post('cont_person');
  $params['reg_date']=$this->input->post('rege_date');
  $params['purpose']=$this->input->post('purpose');
  $contact=$this->input->post('contact');
  for($i=0;$i<count($contact);$i++){
    $data_c[]= $contact[$i];
  }
  $params['contact_no']=implode(",",$data_c);

$ckeckreg=$this->Management_m->checkreg($id, $params['reg_no']);
if($ckeckreg){
  $this->session->set_flashdata('error','Registration number alrady used');
  redirect('Management/editprofile');
}
else{
  $update=$this->Management_m->updateprofile($id,$params);
  $this->session->set_flashdata('success','Succcessfully edited profile');
  redirect('Login/viewprofile');
}

}


}?>