

<style>
  #nav_content .dropdown-toggle
{
    background-color:#74AFAD;

  color:white !important;
}
</style>


<br>
<div class="container">
<h3 style="text-align:center">Add Customer Form</h3>
    <form id="add_user" method="post" action="<?php echo base_url('Customer/addcustomer'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>

        <div class="form-group">
           <label for="organization name" class="col-sm-2 control-label">Organization Name</label>
    <div class="col-md-8"><select  name="organization_id" class="form-control" >
    
    <?php foreach($org as $value){
    

        echo "<option value=\"$value->org_id\">" . $value->name. "</option>";}?>
    </select>
        
    </div>
        </div>

         <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Customer CSV File</label>
            <div class="col-sm-8">
                <input type="file" class="form-control" name="csv" id="csv" required/>
            </div>
        </div>


      

        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
    </form>
</div>


