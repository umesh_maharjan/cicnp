<br>
<H3 style="text-align:center"> Customer List </H3>
<div class="container">

	<?php 
	if($this->session->flashdata('success')){
		echo '<div class="alert alert-success">';
		echo $this->session->flashdata('success');
		echo '</div>';
	}

	?>
	<?php 
	if($this->session->flashdata('error')){
		echo '<div class="alert alert-danger">';
		echo $this->session->flashdata('error');
		echo '</div>';
	}

	?>
	<div class="row" style="align:center;">
		
		<div class="col-md-12"><form action="<?php echo base_url('OrganizationCustomer/searchlist');?>" method="post">
			<table>
				<td class="col-md-4" >
					<td class="col-md-3">Search customer by name
				</td>

				<td class="col-md-3" > <input type="text" name="search_item"  class="form-control" placeholder="Search" minlength="3" required></td>
				<td> <input type="submit"   value="Search" class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;"></td>
			</table>
		</form>

	</div></div>

	<div class="row">
		
		<div class="col-md-12" style="text-align:right"> <a href="<?php echo base_url();?>OrganizationCustomer/add" class="btn btn-success btn-md" ><i class="fa fa-plus"></i>Add Customer</a></div>
		
	</div>
	<table class=" table table-bordered table-hover table-responsive" style="margin-top:20px;">
	 <form id="myform" action="<?php echo base_url();?>OrganizationCustomer/sentsmetoall" method="post" enctype="multipart/form-data">
		<tr>
		<th>Select</th>
			<th>S.N</th>
			<th>Customer Code</th>
			<th>Organization Name</th>

			<th>Customer Name</th>
			<th>Address</th>
			<th>Contact Number</th>
				<th>Action</th>
			

		
		</tr>
		<?php 

		if(!empty($customer)){


			$sn = 1;
			foreach($customer as $r){ 
$codearray= explode('/', $r->customer_id);


?>
			<tr> 
<th><input type='checkbox' class="checkbox-inline" name='action_to[]' id='<?php echo $r->id; ?>' value='<?php echo $r->id; ?>' />
</th>
				<th> <?php echo $sn?>
				</th>
				<th> <?php echo array_values($codearray)[1];?>
				</th>
				

				<th> <?php echo ucwords($r->name);?>
				</th>
				<th> <?php echo $r->customer_name;?>
				</th>
				

				<th> <?php echo ucwords($r->address);?>
				</th>
				<th> <?php echo ucwords($r->contact);?>
				</th>

			
			<th style="padding:6px"><a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php  echo base_url();?>OrganizationCustomer/editview?id=<?php echo $r->id?>">Edit</i></i></a> 
			<a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php  echo base_url();?>OrganizationCustomer/sendsms?id=<?php echo $r->id?>">Send sms</i></i></a> 
				<a class="btn btn-danger btn-md"style="padding:8px 20px 8px 20px;" href="<?php  echo base_url();?>OrganizationCustomer/delete?id=<?php echo $r->id?>">Delete</i></i></a> 
				</th>
		

			</tr>
			<?php  $sn++;} } else {
				?>
			</table>
			<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
				There is no  Customer</div>'

				<?php } ?>
					<th><input type="submit" style="border:0; padding:10px" value="Sent sms to selected customer" class="btn-success" /> </div>
</th>
<table>	
			<tr>
				<td><?php echo $links; ?></td></tr>	
				</table>
			</div>
			<script type="text/javascript">
				document.getElementById('nav_user').className += "active";
			</script>
