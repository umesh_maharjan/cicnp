<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balance extends CI_Controller {

  function __construct()
  {
    
   parent:: __construct();
   $this->load->helper(array('form','url','html','date'));
   $this->load->model('Balance_m');
  	$this->load->library('pagination');
 }

 function index(){
 		if(!$this->session->userdata('login')){
		redirect('Login');

	}
 	$this->load->view('header');
	$this->load->view('navigation');
	$total_row = $this->Balance_m->totalorganization();
	$config = array();



$config["base_url"] = base_url() . "Balance/index";
	$config['use_page_numbers']  = TRUE;
	$config["per_page"] = 1;
	$config['total_rows']= $total_row;
	$config['full_tag_open'] = '<ul class="pagination">';
	$config['full_tag_close'] = '</ul>';
	$config['first_link'] = false;
	$config['last_link'] = false;
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['prev_link'] = '&laquo';
	$config['prev_tag_open'] = '<li class="prev">';
	$config['prev_tag_close'] = '</li>';
	$config['next_link'] = '&raquo';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$this->pagination->initialize($config);
	if($this->uri->segment(3)){
		$page = ($this->uri->segment(3)-1) ;
	}
	else{
		$page = 0;
	}

	$data['sn']=($config["per_page"]*$page)+1;
	$data["links"] = $this->pagination->create_links();

	$data['balance']=$this->Balance_m->getinfo($config["per_page"],$page);

	$this->load->view('balance/balance',$data);
	
 }

 function addview(){
 	if(!$this->session->userdata('login')){
		redirect('Login');

	}
 	$this->load->view('header');
	$this->load->view('navigation');
	$data['org']=$this->Balance_m->getorglist();
		$this->load->view('balance/add_balance',$data);
 }

 function addbalance(){
 	$params=array();
 	$params['org_id']=$this->input->post('organization_id');
 	$params['total_balance']=$this->input->post('total_balance');
 	$params['persms']=$this->input->post('price_per');
 	$params['totalcontact']=$this->input->post('contact_sms');
 	 	$params['created_date']=date('Y-m-d');
 

$checkorg=$this->Balance_m->checkorg($params['org_id']);

if($checkorg){
	$this->session->set_flashdata('error','Organization already added' );  
		redirect('Balance');	
}
else{
	 $data=$this->Balance_m->insert($params);
 	 if($data){

		$this->session->set_flashdata('success','Data Added Successfully' );  
		redirect('Balance');
 	 }
 	 else{
 	 			$this->session->set_flashdata('error','please try again' );  
		redirect('Balance');
 	 }
 	}
 }
function edit_view(){
	$id=$this->input->get('id');

	echo $id;
	$data['org']=$this->Balance_m->getorglist();
	$data['editdata']=$this->Balance_m->geteditdata($id);


		$this->load->view('header');
	$this->load->view('navigation');

		$this->load->view('balance/edit_balance',$data);
}
function editbalance(){
	$params=array();

	$id=$this->input->post('id');
 	$params['org_id']=$this->input->post('organization_id');
 	$params['total_balance']=$this->input->post('total_balance');
 	$params['persms']=$this->input->post('price_per');
 	$params['totalcontact']=$this->input->post('contact_sms');
 	 	$params['created_date']=date('Y-m-d');
 	 $data=$this->Balance_m->update($params,$id);
 	 if($data){

		$this->session->set_flashdata('success','Data update Successfully' );  
		redirect('Balance');
 	 }
 	 else{
 	 			$this->session->set_flashdata('error','please try again' );  
		redirect('Balance');
 	 }
}
function addbalanceview(){
	$data['id']=$this->input->get('id');
		if(!$this->session->userdata('login')){
		redirect('Login');

	}
 	$this->load->view('header');
	$this->load->view('navigation');
$this->load->view('balance/addbalance',$data);

}

function addorgbalance(){
	$id=$this->input->post('id');


	$total_balance=$this->input->post('add_balance');
	$pre_balance=$this->Balance_m->geteditdata($id);
	foreach($pre_balance as $r){
		$balance=$r->total_balance;
	}

$params['total_balance']=$balance+$total_balance;
	$params['created_date']=date('Y-m-d');
 $data=$this->Balance_m->update($params,$id);
 	 if($data){

		$this->session->set_flashdata('success','Data update Successfully' );  
		redirect('Balance');
 	 }
 	 else{
 	 			$this->session->set_flashdata('error','please try again' );  
		redirect('Balance');
 	 }

	

}


function delete(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
 	$this->load->view('header');
  $data['id']=$this->input->get('id');
  $data['org']=$this->Balance_m->geteditdata($data['id']);

  $this->load->view('Balance/delete_balance',$data);
}


function confirm_delete(){
  $id=$_POST['id'];
  

  $data=$this->Balance_m->delete($id);
  $this->session->set_flashdata('success', 'Delete successfully');
  redirect('Balance'); 
    //}
     // else{
  $this->session->set_flashdata('error', ' Server error  Please try again !');
  redirect('Balance'); 
  

}
}
