<br>
<div class="container">

<h3 style="text-align:center">Add Blacklist Form</h3>

<form id="add_user" method="post" action="<?php echo base_url('blacklist/addblacklist'); ?>" class="form-horizontal"  enctype="multipart/form-data">

<?php 

if($this->session->flashdata('success')){
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('success');
    echo '</div>';
}
if($this->session->flashdata('error')){
    echo '<div class="alert alert-danger">';
    echo $this->session->flashdata('error');
    echo '</div>';
}
?>

    <div class="form-group">

    <label for="firstname" class="col-sm-2 control-label"> Name</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="name" id="name" style="text-transform: capitalize;" required/>
    </div>  
    </div>

      <div class="form-group">

    <label for="firstname" class="col-sm-2 control-label"> Father Name</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="f_name" id="f_name"  style="text-transform: capitalize;"  required/>
    </div>  
    </div>
      <div class="form-group">

    <label for="firstname" class="col-sm-2 control-label"> Grandfather Name</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="g_name" id="g_name" style="text-transform: capitalize;" required/>
    </div>  
    </div>

    <div class="form-group">
    <label  class="col-sm-2 control-label"> Permanent Address</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="p_address" style="text-transform: capitalize;"  id="address" required />
    </div>
    </div>

     <div class="form-group">
            <label for="contact" class="col-sm-2 control-label"> Contact Number</label>
            <div class="col-sm-7">
                <input type="text" pattern="[0-9]{7,10}" class="form-control" name="contact[]" id="contact"  required/>
            </div>
            <div class="col-sm-0">
                <button class="btn btn-default" id="add_more_contact" onclick="addContact()">Add More</button>
            </div>
        </div> 

   <div id="add_contact"></div>    



    <div class="form-group">
    <label for="citizen number" class="col-sm-2 control-label">Cititzen Number</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" name="citizen_no" id="citizen_no"  required/>
    </div>
    </div>
   


    <div class="form-group">
    <label  class="col-sm-2 control-label">Citizen Issued Place</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="citizen_place"  id="citizen_place" required />
    </div>
    </div>
  
    
    <div class="form-group">
    <label  class="col-sm-2 control-label">Birth of date</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" name="bod"  id="bod" required />
    </div>
    </div>
    
        <div class="form-group">
    <label  class="col-sm-2 control-label">Photo</label>
    <div class="col-sm-8">
        <input type="file" class="form-control" name="image"  id="image" required />
    </div>
    </div>

        <div class="form-group">
    <label  class="col-sm-2 control-label">Upload by </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="upload"  id="upload" required />
    </div>
    </div>




   

   
    <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
</form>
</div>


<script type="text/javascript">
    var count= 0;
    var addContact = function(){
        count = count+1;
        console.log(count);
        $("#add_contact").append("<div class='container' id='contact"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' pattern='[0-9]{7,10}' class='form-control' name='contact[]'   placeholder='Contact'    required/></div><div class='col-sm-0'><button class='btn btn-default' id='contact"+count+"' onclick='removeContact("+count+")'>Remove</button></div></div>");
    };

    var removeContact = function(c){
        console.log("remove count: "+c);
        console.log("contact"+c);
        $("#contact"+c).remove();
        
    };







   
</script>

