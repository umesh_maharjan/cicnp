
<script type="text/javascript">
  $(window).load(function(){
    $('#myModal').modal('show');
  });
</script>
<?php 
foreach($users as $r){
  $name = $r->name;
  $photo = $r->photo;
  
}

?>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure you want to delete the user?</h4>
      </div>
      
      <div class="modal-body">
        <p>
          Name: <?php echo $name;
          ?>
        </p>

        <p>
          <img src="<?php echo base_url();?>uploads/black/<?php echo $photo;?>" width="100" height="100">
          
        </p>
        <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="delete_user()">OK</button>
        <button type="button" class="btn btn-default" onclick="reload()">Cancel</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
	var delete_user = function(){
		var id = document.getElementById('id').value;
		$.ajax({
			url: 'confirm_delete',
			data: { id:id },
			type: 'POST',
			success: function(response){
        console.log(id);
        window.location.href = "index";
      }
    });	
	}

	var reload = function(){
		window.location.href = "index";
	}
</script>


