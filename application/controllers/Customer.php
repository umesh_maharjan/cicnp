<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

  function __construct()
  {
    
   parent:: __construct();
   $this->load->helper(array('form','url','html','date'));
   $this->load->model('Customer_m');
      $this->load->model('Balance_m');
        $this->load->library('pagination');
  $this->load->library('csvimport');
 }

 function index(){
 			if(!$this->session->userdata('login')){
		redirect('Login');

	}
 	$this->load->view('header');
	$this->load->view('navigation');

$total_row = $this->Customer_m->totalcustomer();
  $config = array();
$config["base_url"] = base_url() . "Customer/index";
  $config['use_page_numbers']  = TRUE;
  $config["per_page"] = 1;
  $config['total_rows']= $total_row;
  $config['full_tag_open'] = '<ul class="pagination">';
  $config['full_tag_close'] = '</ul>';
  $config['first_link'] = false;
  $config['last_link'] = false;
  $config['first_tag_open'] = '<li>';
  $config['first_tag_close'] = '</li>';
  $config['prev_link'] = '&laquo';
  $config['prev_tag_open'] = '<li class="prev">';
  $config['prev_tag_close'] = '</li>';
  $config['next_link'] = '&raquo';
  $config['next_tag_open'] = '<li>';
  $config['next_tag_close'] = '</li>';
  $config['last_tag_open'] = '<li>';
  $config['last_tag_close'] = '</li>';
  $config['cur_tag_open'] = '<li class="active"><a href="#">';
  $config['cur_tag_close'] = '</a></li>';
  $config['num_tag_open'] = '<li>';
  $config['num_tag_close'] = '</li>';
  $this->pagination->initialize($config);
  if($this->uri->segment(3)){
    $page = ($this->uri->segment(3)-1) ;
  }
  else{
    $page = 0;
  }

  $data['sn']=($config["per_page"]*$page)+1;
  $data["links"] = $this->pagination->create_links();




	$data['customer']=$this->Customer_m->getcustomer($config["per_page"],$page);
	$this->load->view('customer/customerlist',$data);
	
 }

 function addcustomerview(){
 	if(!$this->session->userdata('login')){
		redirect('Login');

	}	
		$this->load->view('header');
	$this->load->view('navigation');
		$data['org']=$this->Balance_m->getorglist();
		$this->load->view('customer/add_customer',$data);
 }

 function addcustomer(){
 	 		$data =array();
         		$config['upload_path'] = './uploads/customer';
             $config['allowed_types'] = 'text/plain|text|csv|csv';
             $config['encrypt_name'] = TRUE;
             $config['max_size'] = '100000';
             $config['overwrite']    =  TRUE;
          $this->load->library('upload', $config);
         $this->upload->initialize($config);
			 
 		if(!$this->upload->do_upload('csv'))
       
       	 {
       	 
             $this->session->set_flashdata('error', $this->upload->display_errors());  
       	 }
       	 else{
       	 	//ini_set('max_execution_time', 1200);
       	 	  $file_data = $this->upload->data();
                
                       $data['file'] = $file_data['file_name'];
                 $data['created_date']=date('Y-m-d');
               	$data['org_id']=$this->input->post('organization_id');
                      
                       $fileinsert=$this->Customer_m->insert_file($data);
                      if($fileinsert){
                      	$file_path =  './uploads/customer/'.$file_data['file_name'];

                      if ($this->csvimport->get_array($file_path)) {

                $csv_array = $this->csvimport->get_array($file_path);
                foreach ($csv_array as $row) {

                    $insert_data = array(
                        'customer_name'=>$row['customer_name'],
             
                        'org_id'=>$data['org_id'],
                       'address'=>$row['address'],
                         'contact'=>$row['contact'],
                        'customer_id'=>$row['customer_id'],
                        'created_date'=> $data['created_date']
                        
                    );
            
                   $inseripodata=$this->Customer_m->insert($insert_data);
                 
                  
                    
                     			 }
                  			}
       					 }
       					 $this->session->set_flashdata('success', 'Add  customer Successfully');
    				 		redirect('Customer');
      				   }
 }

 function searchlist(){
 	$searchtype = $this->Customer_m->searchterm_handler($this->input->get_post('search', TRUE));
 //==$searchtype=$this->input->post('search');
// $searchvalue=$this->input->post('search_item');
$searchvalue = $this->Customer_m->searchvalue_handler($this->input->get_post('search_item', TRUE));
if($this->input->get_post('search') && $this->input->get_post('search_item')){
  redirect('Customer/searchlist');
}

 if($searchtype=="0"){
   $this->session->set_flashdata('error','Please select type');
   redirect('Customer');
 }
 else if($searchtype=="1"){
    $data['customer']=$this->Customer_m->getcustomerbyorg($searchvalue);


 
   $this->load->view('header');
   $this->load->view('navigation');
   $this->load->view('customer/customerlist',$data);
   

 }
 else{
    $data['customer']=$this->Customer_m->getcustomerbyname($searchvalue);
   $this->load->view('header');
   $this->load->view('navigation');
   $this->load->view('customer/customerlist',$data);

 }


 }
//sms log function
 function smslog(){
   
    if(!$this->session->userdata('login')){
    redirect('Login');

  }
  $data['smslog']=$this->Customer_m->smslog(); 
 ;
   $this->load->view('header');
   $this->load->view('navigation');
   $this->load->view('sms/smslog',$data);
 }

}