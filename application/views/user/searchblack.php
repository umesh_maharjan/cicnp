

<br><div class="container">

<h3 style="text-align:center">Search Blacklist </h3>

<?php 
if($this->session->flashdata('success')){
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

?>
<?php 
if($this->session->flashdata('error')){
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('error');
	echo '</div>';
}

?>
<div class="row" style="text-align:center;">

	<div><form action="<?php echo base_url('Blacklist/blacklistsearch');?>" method="post">
		<table>
			<td class="col-md-4" ></td>
			<td class="col-md-3" ><select class="form-control"  name="search">

				<option value="0">--Select type--</option>
				<option value="1">Name</option>
				<option value="2">Citizen number</option>
			</select>
		</td>

		<td class="col-md-3" > <input type="text" name="search_item"  class="form-control" placeholder="Search" required></td>
		<td> <input type="submit"   value="Search" class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;"></td>
	</table>
</form>

</div>
</div>


<table class=" table table-bordered table-hover table-responsive" style="margin-top:20px;">
	<?php if(!empty($users)) { ?>
	<tr>
		<th>S.N</th>
		<th>Photo</th>

		<th>Name</th>
		<th>Citizen Number</th>
		<th>Contact Number</th>
		<th>Action</th>
		
		

	</tr>

	<?php	
	$sn = 1;
	foreach($users as $r){ ?>
	<tr> 

		<th> <?php echo $sn?>
		</th>
<?php if($r->organization_id==0) { ?>
<th> <img src="<?php echo base_url();?>uploads/black/2960579f1a04ac292.png" width="80" height="80">
<th> <?php echo $r->name;?>
</th>
<th> <?php echo $r->permanent_address;?>
</th>
<th>N/A
</th>
<th>N/A
</th>
<?php } else { ?>

		<th> <img src="<?php echo base_url();?>uploads/black/<?php echo $r->photo;?>" width="80" height="80">
		</th>

		<th> <?php echo $r->name;?>
		</th>
		<th> <?php echo $r->citizen_number;?>
		</th>
		<th> <?php echo $r->contact_no;?>
		</th>
		<th style="padding:6px"><a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php  echo base_url();?>blacklist/userdetail?id=<?php echo $r->black_id;?>">Detail</a> 

		<a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php  echo base_url();?>blacklist/info?id=<?php echo $r->organization_id;?>&bd_id=<?php echo $r->black_id;?>">Inform</a>

		</th>
		<?php }?>
		
	</tr>

	<?php $sn++; } }  else {?>
</table>


<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
	There is no   blacklist</div>
	<?php } ?>






</div>