<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blacklist extends CI_Controller {

  function __construct()
  {
    
   parent:: __construct();
   $this->load->helper(array('form','url','html','date'));
   $this->load->model('Black_m');
   $this->load->model('Common_m');
   $this->load->model('Management_m');
     $this->load->library('csvimport');
 }
 public function index(){

 	if(!$this->session->userdata('login')){
 		redirect('Login');
 	}
  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);
  $data['count']=$totalloanapplied-$totalviewed;
  $this->load->view('header');
  $this->load->view('menu',$data);	
  $data['users']=$this->Black_m->getusers($id);
  
  $this->load->view('user/userlist',$data);

}
public function  searchlist(){
 $id=$this->session->userdata('id');

$searchtype = $this->Management_m->searchterm_handler($this->input->get_post('search', TRUE));
 //==$searchtype=$this->input->post('search');
// $searchvalue=$this->input->post('search_item');
$searchvalue = $this->Management_m->searchvalue_handler($this->input->get_post('search_item', TRUE));
if($this->input->get_post('search') && $this->input->get_post('search_item')){
  redirect('Blacklist/searchlist');
}

 $totalloanapplied=$this->Management_m->getloancount();
 $totalviewed=$this->Management_m->getnotificationcount($id);
 $data['count']=$totalloanapplied-$totalviewed;


 if($searchtype=="0"){
   $this->session->set_flashdata('error_select','Please select type');
   redirect('Blacklist');
 }
 else if($searchtype=="1"){

   $data['users']=$this->Black_m->getusersname($id,$searchvalue);

 
   $this->load->view('header');
   $this->load->view('menu',$data);
   $this->load->view('user/userlist',$data);
   

 }
 else{
   $data['users']=$this->Black_m->getusercitizen($id,$searchvalue);
   $this->load->view('header');
   $this->load->view('menu',$data);
   $this->load->view('user/userlist',$data);

 }

}


public function add_view(){
 if(!$this->session->userdata('login')){
   redirect('Login');
 }


 $id=$this->session->userdata('id');

 $totalloanapplied=$this->Management_m->getloancount();
 $totalviewed=$this->Management_m->getnotificationcount($id);
 $data['count']=$totalloanapplied-$totalviewed;
 $this->load->view('header');
 $this->load->view('menu',$data);
 $this->load->view('user/add_userdetail');

}
public function addblacklist(){
 $params=array();
 $params['name']=$this->input->post('name');

 $params['father_name']=$this->input->post('f_name');

 $params['grandfather_name']=$this->input->post('g_name');

 $params['permanent_address']=$this->input->post('p_address');

 $params['bod']=$this->input->post('bod');

 //$params['contact_no']=$this->input->post('contact');

 $params['citizen_number']=$this->input->post('citizen_no');

 $params['citizen_issued_place']=$this->input->post('citizen_place');

 $params['organization_id']=$this->session->userdata('id');


 $params['upload_by']=$this->input->post('upload');
 $params['created_on']=now();

 $checkcitizen=$this->Black_m->checkcitizen($params['citizen_number'],$params['organization_id']);
   $contact=$this->input->post('contact');
  for($i=0;$i<count($contact);$i++){
   $data_c[]= $contact[$i];
 }

 $params['contact_no']=implode(",",$data_c);


 $config['upload_path'] = './uploads/black';
 $config['allowed_types'] = 'gif|jpg|png';
 $config['overwrite']    =  TRUE;
 
				//$config['encrypt_name'] = true;
 
 $config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
				// $config['max_height']  = '720';   
 $config['overwrite']    =  TRUE;
 $config['file_name']  = uniqid(rand());	
 $this->load->library('upload', $config);
 $this->upload->initialize($config);

 $type= explode('.',$_FILES['image']['name']);
 $type= $type[count($type)-1];
 
 $params['photo'] = $config['file_name'].'.'.$type;

 
 $upload = $this->upload->data();
 

 if(!$this->upload->do_upload('image'))
   
 {


   $error = array('error' => $this->upload->display_errors());              
   $this->session->set_flashdata('error', $this->upload->display_errors());  
   redirect('Blacklist/add_view');
 }
 else if($checkcitizen){
  $this->session->set_flashdata('error','Citizen number already used');
  redirect('Blacklist/add_view');
}
else{
  $data=$this->Black_m->insert($params,'user_detail');

  if($data){
    $this->session->set_flashdata('success','Successfully add blacklist');
    redirect('Blacklist');

  }
  else{
   $this->session->set_flashdata('error','Server error please try it again !');
   redirect('Blacklist/addview');
 }


}
}

public function changepasswordorg(){
  if(!$this->session->userdata('login')){
    redirect('Login');
  }

  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);
  $data['count']=$totalloanapplied-$totalviewed;
  
  $this->load->view('header');
  $this->load->view('menu',$data);

  $this->load->view('user/changepassword');
}




public  function search_blacklist(){

  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);
  $data['count']=$totalloanapplied-$totalviewed;
  $this->load->view('header');
  $this->load->view('menu',$data);
  $this->load->view('user/searchblack');

}


public function  blacklistsearch(){

$searchtype = $this->Management_m->searchterm_handler($this->input->get_post('search', TRUE));
 //==$searchtype=$this->input->post('search');
// $searchvalue=$this->input->post('search_item');
$searchvalue = $this->Management_m->searchvalue_handler($this->input->get_post('search_item', TRUE));
if($this->input->get_post('search') && $this->input->get_post('search_item')){
  redirect('Blacklist/blacklistsearch');
}
  // $searchtype=$this->input->post('search');

  // $searchvalue=$this->input->post('search_item');

  $id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($id);
  $data['count']=$totalloanapplied-$totalviewed;


  if($searchtype=="0"){
    $this->session->set_flashdata('error_select','Please select type');
    redirect('Blacklist/search_blacklist');
  }
  else if($searchtype=="1"){

    $data['users']=$this->Black_m->getallblacklistname($searchvalue);
    if($data['users']){

      $this->load->view('header');
      $this->load->view('menu',$data);
      $this->load->view('user/searchblack',$data);
    }
    else{
     $this->session->set_flashdata('error_empty','There is no blacklist');
     redirect('Blacklist/search_blacklist');
     
   }

 }
 else{
  $data['users']=$this->Black_m->getblacklistcitizen($searchvalue);
  if($data['users']){
    $this->load->view('header');
    $this->load->view('menu',$data);
    $this->load->view('user/searchblack',$data);
  }
  else{
    $this->session->set_flashdata('error_empty','There is no blacklist');
    redirect('Blacklist/search_blacklist');

  }

}

}


function userdetail(){
  if(!$this->session->userdata('login')){
    redirect('Login');
  }


  $org_id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($org_id);
  $data['count']=$totalloanapplied-$totalviewed;

  $id=$this->input->get('id');
  $data['black']=$this->Black_m->getuserdetail($id);
  $this->load->view('header');
  $this->load->view('menu',$data);
  $this->load->view('user/userdetail',$data);
}


function updatephotoview(){
 if(!$this->session->userdata('login')){
  redirect('Login');
}

$org_id=$this->session->userdata('id');

$totalloanapplied=$this->Management_m->getloancount();
$totalviewed=$this->Management_m->getnotificationcount($org_id);
$data['count']=$totalloanapplied-$totalviewed;
$this->load->view('header');
$this->load->view('menu',$data);
$this->load->view('user/changephoto');


}

function updatephoto(){
 if(!$this->session->userdata('login')){
  redirect('Login');
}
$id=$this->session->userdata('id');




$config['upload_path'] = './uploads/bodphoto';
$config['allowed_types'] = 'gif|jpg|png';
$config['overwrite']    =  TRUE;

        //$config['encrypt_name'] = true;

$config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
        // $config['max_height']  = '720';   
$config['overwrite']    =  TRUE;
$config['file_name']  = uniqid(rand());  
$this->load->library('upload', $config);
$this->upload->initialize($config);

$type= explode('.',$_FILES['image']['name']);
$type= $type[count($type)-1];

$params['bod_photo'] = $config['file_name'].'.'.$type;


$upload = $this->upload->data();


if(!$this->upload->do_upload('image'))
 
{


  $error = array('error' => $this->upload->display_errors());              
  $this->session->set_flashdata('error', $this->upload->display_errors());  
}
else{



  $getimage=$this->Black_m->getimage($id);
  
  foreach($getimage as $i){
    unlink ("./uploads/bodphoto/".$i->bod_photo );
  }
  $data=$this->Common_m->update($id,$params,'cooperative_list');


  if($data){
    $this->session->set_flashdata('success','Successfully photo changed');
    redirect('Login/viewprofile');
  }
  else{
    $this->session->set_flashdata('error','Server error please try again !');
    redirect('Login/viewprofile');
  }






}

}

function edit(){

  if(!$this->session->userdata('login')){
    redirect('Login');
  }


  $org_id=$this->session->userdata('id');

  $totalloanapplied=$this->Management_m->getloancount();
  $totalviewed=$this->Management_m->getnotificationcount($org_id);
  $data['count']=$totalloanapplied-$totalviewed;
  $id=$this->input->get('id');
  $this->load->view('header');
  $this->load->view('menu',$data);

  $data['black']=$this->Black_m->getuserdetail($id);
  $this->load->view('user/edit_userdetail',$data);
  

}

function editblacklist(){


  $params=array();
  $id=$this->input->post('id');
  $params['name']=$this->input->post('name');

  $params['father_name']=$this->input->post('f_name');

  $params['grandfather_name']=$this->input->post('g_name');

  $params['permanent_address']=$this->input->post('p_address');

  $params['bod']=$this->input->post('bod');

 // $params['contact_no']=$this->input->post('contact');

  $params['citizen_number']=$this->input->post('citizen_no');

  $params['citizen_issued_place']=$this->input->post('citizen_place');

  $params['organization_id']=$this->session->userdata('id');


$params['upload_by']=$this->input->post('upload');
  $params['created_on']=now();

    $contact=$this->input->post('contact');
  for($i=0;$i<count($contact);$i++){
   $data_c[]= $contact[$i];
 }
 $params['contact_no']=implode(",",$data_c);

  $chckcitizen=$this->Black_m->chkcitizen($id,$params['citizen_number']);

  if($chckcitizen){
    $this->session->set_flashdata('error','citizen number already used');
    redirect('blacklist/edit?id='.$id);
  }
  else{
    $config['upload_path'] = './uploads/black';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['overwrite']    =  TRUE;
    
        //$config['encrypt_name'] = true;
    
    $config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
        // $config['max_height']  = '720';   
    $config['overwrite']    =  TRUE;
    $config['file_name']  = uniqid(rand());  
    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    $type= explode('.',$_FILES['image']['name']);
    $type= $type[count($type)-1];
    
    $params['photo'] = $config['file_name'].'.'.$type;

    
    $upload = $this->upload->data(); 

    if(!$this->upload->do_upload('image'))
     
    {

      $getphoto=$this->Black_m->getphoto($id);
      foreach($getphoto as $p);{
        
      }

      $params['photo']=$p->photo;  

      $data=$this->Black_m->update($id,$params,'user_detail');
      if($data){
       $this->session->set_flashdata('success','Successfully edited blacklist');
       redirect('Blacklist'); 
     }
   }
   else{

    $getphoto=$this->Black_m->getphoto($id);
    foreach($getphoto as $p);{
      unlink ("./uploads/black/".$p->photo );
    }
    $data=$this->Black_m->update($id,$params,'user_detail');
    if($data){
     $this->session->set_flashdata('success','Successfully edited blacklist');
     redirect('Blacklist'); 
   }


 }

}


}

function delete(){
  if(!$this->session->userdata('login')){
    redirect('Login');
  }
  $data['id']=$this->input->get('id');
  $data['users']=$this->Black_m->getblack_id($data['id']);



  $this->load->view('header');


  $this->load->view('user/delete_blacklist',$data);
}


function confirm_delete(){
  $id=$_POST['id'];
  
  $params=$this->Black_m->getphoto($id);
  foreach($params as $p);{
    unlink ("./uploads/black/".$p->photo );
  }

  $data=$this->Black_m->deleteuser($id);
  $this->session->set_flashdata('success', 'Delete successfully');
  redirect('Blacklist'); 
    //}
     // else{
  $this->session->set_flashdata('error', ' Server error  Please try again !');
  redirect('Blacklist'); 
  
}

public function info(){
  $bd_id=$this->input->get('bd_id');

  $id=$this->input->get('id');
  $org_id=$this->session->userdata('id');
  $orginfo=$this->Black_m->getorginfo($org_id);

foreach($orginfo as $i){
	  $orgname=$i->name;
  $address=$i->address;
  $contactfromarray = explode(',', $i->contact_no);
}

  $balckuser=$this->Black_m->getblack_id($bd_id);
  foreach($balckuser as $b){
    $bdname=$b->name;
    $citizen_number=$b->citizen_number;
  }

  $getemail=$this->Black_m->getemail($id);

foreach($getemail as $r){
  $email=$r->email;

  
  $contactarray = explode(',', $r->contact_no);
}


$message="Blacklist Detail \n
 Blacklist Name: ".$bdname."\n Citizenship No.:".$citizen_number."\nFrom\nName:".$orgname."\nAddress:".$address."\nPhone:". array_values($contactfromarray)[0];

 
  
  $api_url = "http://api.sparrowsms.com/v2/sms/?".
        http_build_query(array(
            'token' => 'Fenlprslyx0h62REadIp',
           'from'  => 'Demo',
            'to'    => array_values($contactarray)[0],
            'text'  => $message));

    $response = file_get_contents($api_url);
    // print_r($response);die;

//echo $email;
//echo $email;
$this->load->library('email');
    $params['email']=$email;


//$comfirmpass=$this->input->post('confirm_password');


    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'gator4164.hostgator.com';
    $config['smtp_port']    = '587';
    $config['smtp_timeout'] = 5;
    $config['smtp_user']    = 'admin@cicnp.com';
    $config['smtp_pass']    = 'admin@cic123##';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";

    $config['mailtype'] = 'html'; 
    $config['validation'] = TRUE;

    $this->email->initialize($config);
    $this->email->from('admin@cicnp.com', 'cicnp');
    $this->email->to($params['email']);
    $this->email->subject('Information about blacklist');

    $contact='<br clear="all" />
   Inform organization success ';


      $this->email->message($contact);
          //  $this->email->send();


      if(!$this->email->send())
      {
       // echo $this->email->print_debugger(); die;
        $this->session->set_flashdata('error', 'Server error please try again!');
        redirect('Blacklist/blacklistsearch');


      }
      else
      {
        $this->session->set_flashdata('success', 'Email sent successfully');
        redirect('Blacklist/blacklistsearch');
      }


}

function addbacklist(){
   if(!$this->session->userdata('login')){
    redirect('Login');
  }
    $this->load->view('header');
    $this->load->view('navigation');
   $this->load->view('user/addblacklist');
}

function add(){
     ini_set('max_execution_time', 300);
      $data =array();
            $config['upload_path'] = './uploads/blacklist';
             $config['allowed_types'] = 'text/plain|text|csv|csv';
             $config['encrypt_name'] = TRUE;
             $config['max_size'] = '100000';
             $config['overwrite']    =  TRUE;
          $this->load->library('upload', $config);
         $this->upload->initialize($config);
       
    if(!$this->upload->do_upload('csv'))
       
         {
         
             $this->session->set_flashdata('error', $this->upload->display_errors());  
         }
         else{
          //ini_set('max_execution_time', 1200);
            $file_data = $this->upload->data();
                
                       $data['file'] = $file_data['file_name'];
                  $data['created_date']=date('Y-m-d');
                
                      
                       $fileinsert=$this->Black_m->insert($data,'blacklist_file');
                      if($fileinsert){
                        $file_path =  './uploads/blacklist/'.$file_data['file_name'];

                      if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                $delete=$this->Black_m->deleteblacklist();
                if($delete){
                foreach ($csv_array as $row) {
                    $insert_data = array(
                        'name'=>$row['name'],
                       'permanent_address'=>$row['address'],
                       'organization_id'=>'0',  
                    );
            
                   $inseripodata=$this->Black_m->insert($insert_data,'user_detail');
                 
                  
                    
                           }
                        }
                      }
                 }
                 $this->session->set_flashdata('success', 'Add  blacklist Successfully');
                redirect('Login/getblacklist');
                 }
 }
}
?>