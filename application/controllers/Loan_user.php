<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_user extends CI_Controller{
  function __construct(){
   parent :: __construct();
   $this->load->helper(array('form','url','html','date'));
   $this->load->model('Loan_user_m');
   $this->load->model('Common_m');
 }

 public function index(){
   $this->load->view('header');
   $this->load->view('loan_user/loan_form');
 }

 public function adduser(){

         
  $data['name'] = $this->input->post('name');
  $data['address'] = $this->input->post('address');
  $params['Gender'] = $this->input->post('gender');
  $params['DOB'] = $this->input->post('dob');
  $data['Email'] = $this->input->post('email');
  $params['CitizenshipNO'] = $this->input->post('citizen');
  $params['CitizenshipIssued'] = $this->input->post('citizen_place');
  $data['usertype'] = "Loan";

  $password= $this->input->post('password');
  $c_password = $this->input->post('c_password');
  $params['Occupation'] = $this->input->post('occupation');
  $data['active']="0"; 
  $contact=$this->input->post('contact');
  $education=$this->input->post('education');


  for($i=0;$i<count($contact);$i++){
    $data_c[]= $contact[$i];
  }
  $data['contact_no']=implode(",",$data_c);
			//echo $allcontact;

			//education
  
  for($i=0;$i<count($education);$i++){
    $data_e[]= $education[$i];
  }
  $params['Education']=implode(",",$data_e);

// $b=rand();
//  $key =sha1($b);
  $data['salt']=substr(md5(uniqid(rand(), true)), 0, 6);
  $data['password']=sha1($password.$data['salt'].'Db_Project');
  $checkmail=$this->Loan_user_m->checkemail($data['Email']);

  $checksitizen=$this->Loan_user_m->checkcitizen($params['CitizenshipNO']);


 $config['upload_path'] = './uploads/loan';
 $config['allowed_types'] = 'gif|jpg|png';
 $config['overwrite']    =  TRUE;
 
        //$config['encrypt_name'] = true;
 
 $config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
        // $config['max_height']  = '720';   
 $config['overwrite']    =  TRUE;
 $config['file_name']  = uniqid(rand());  
 $this->load->library('upload', $config);
 $this->upload->initialize($config);

 $type= explode('.',$_FILES['image']['name']);
 $type= $type[count($type)-1];
 
 $data['photo'] = $config['file_name'].'.'.$type;

 
 $upload = $this->upload->data();
 

 if(!$this->upload->do_upload('image'))
   
 {


   $error = array('error' => $this->upload->display_errors());              
   $this->session->set_flashdata('error', $this->upload->display_errors());  
   redirect('Loan_user');
 }





  else if($checkmail){
    $this->session->set_flashdata('error','Email Already exist');
    
    redirect('Loan_user');
  }

  else if($checksitizen){
   $this->session->set_flashdata('error','Citizen Number already userd');
   
   redirect('Loan_user');
 }
				 	//password check
 else if($password != $c_password){
  $this->session->set_flashdata('error','Password Mismatch');
  
  redirect('Loan_user');
}


else{

 $params=$this->Loan_user_m->adduser($data,$params);


 if($params){

   $this->load->library('email');
  $config['protocol']    = 'smtp';
      $config['smtp_host']    = 'gator4164.hostgator.com';
      $config['smtp_port']    = '587';
      $config['smtp_timeout'] = 5;
        $config['smtp_user']    = 'admin@cicnp.com';
   $config['smtp_pass']    = 'admin@cic123##';
      $config['charset']    = 'utf-8';
      $config['newline']    = "\r\n";
      
      $config['mailtype'] = 'html'; 
      $config['validation'] = TRUE;

      $this->email->initialize($config);
      $this->email->from('admin@cicnp.com', 'cicnp');
      $this->email->to($params['email']);
      $this->email->subject('Please active your account');

   $contact='<br clear="all" />
  
   <br> <a href=http://www.cicnp.com/Loan_user/activeaccount?email='.$data['Email'].'>Click to activate your account<a>';


   $this->email->message($contact);
          //  $this->email->send();
   
   
   if(!$this->email->send())
   {
     
    
   }
   else
   {
    
     
   }

   $this->session->set_flashdata('success','Please check your email to active account');
   redirect('Login');
 }
 else{
   $this->session->set_flashdata('error','Your account has been registered');
   redirect('Login');	
 }
}
}	
function activeaccount(){
 $email=$this->input->get('email');
 $data['Active']="1";
 $active=$this->Loan_user_m->active_account($email,$data);
 $this->session->set_flashdata('success','Your account has been Activited');
 redirect('Login');  
}


function apply_load(){

         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}

 $this->load->view('header');
 $this->load->view('loan_menu');


 $this->load->view('loan_user/loan_user_details');

}
function addapplyloan(){
           if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}
	$params=array();
	$params['FatherName']=$this->input->post('fathername');
	$params['GrandFatherName']=$this->input->post('grandfathername');
	$params['Purpose']=$this->input->post('purpose');
	$params['MonthlyExpense']=$this->input->post('monthlyexpense');
	$params['MOnthlyIncome']=$this->input->post('monthlyincome');

	$params['AppliedLoan']=$this->input->post('appliedloan');
	$params['user_id']=$this->session->userdata('id');

	$Collateral=$this->input->post('collateral');
  for($i=0;$i<count($Collateral);$i++){
   $data_c[]= $Collateral[$i];
 }
 $params['Collateral']=implode(",",$data_c);

 $params['LoanApplied']='0';

 $data=$this->Loan_user_m->applyloan($params);
 if ($data){
  $this->session->set_flashdata('success','Please recheck to apply loan');
  redirect('loan_user/appiledload');
}
else{

}


}


function appiledload(){
          if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}
  $id=$this->session->userdata('id');
  $this->load->view('header');
  $this->load->view('loan_menu');

  $data['loan']=$this->Loan_user_m->loanlist($id);
  $this->load->view('loan_user/loanlist',$data);

}

function applied(){
           if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}
 $id=$this->input->get('id');
 $params['LoanApplied']="1";
 $params['created_on']=now();
 $data=$this->Loan_user_m->updateloanapplied($id,$params);


 if($data){
   $this->session->set_flashdata('success','Successfully applied loan');
   redirect('loan_user/appiledload');	
 }
}



function deleteloan(){
           if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}
  $id=$this->input->get('id');
  $params['LoanApplied']='0';
  $params['archive']='1';
  $items['archive_loan']='1';
  $data=$this->Loan_user_m->deleteupdate($id,$params);
  if($data){

   $data=$this->Loan_user_m->deleteupdatenotify($id,$items);
   $this->session->set_flashdata('success','Successfully applied loan');
   redirect('loan_user/appiledload');	

 }
}

function viewprofile(){
         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}
  $id=$this->session->userdata('id');

  $data['profile']=$this->Loan_user_m->getprofile($id);


  $this->load->view('header');
  $this->load->view('loan_menu');
  $this->load->view('loan_user/loanuser_detail',$data);





}


public function changepasswordorg(){
         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}

  
  $this->load->view('header');
  $this->load->view('loan_menu');

  $this->load->view('loan_user/changepassword');
}


public function updatepassword(){
  $id=$this->session->userdata('id');
  $oldpassword = $this->input->post('old_password');
  $newpassword=$this->input->post('new_password');
  $confirm_pw = $this->input->post('confirm_password');

  $checkoldpassword=$this->Common_m->getorg($id);

  foreach($checkoldpassword as $p){
    $old_pass=$p->password;
    $salt=$p->salt;
    
  }

  $psw=sha1($oldpassword.$salt.'Db_Project');
  if($psw !=$old_pass){
    $this->session->set_flashdata('error', 'Old Password  doesnt match');
    redirect('loan_user/changepasswordorg');
  }else if($oldpassword==$newpassword){
    $this->session->set_flashdata('error', 'New password same as old password');
    redirect('loan_user/changepasswordorg');
  }
  else if($newpassword != $confirm_pw){
    $this->session->set_flashdata('error', 'New password and confirm password doesnt match ');
    redirect('loan_user/changepasswordorg');
  }
  else{
    $data['password']=sha1($newpassword.$salt.'Db_Project');
    $this->Common_m->update($id,$data,'cooperative_list');
    $this->session->set_flashdata('success', 'Password changed successfully ');
    redirect('loan_user/changepasswordorg');
  }
}


function editloanlist(){

	         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}
	$id=$this->input->get('id');


  $this->load->view('header');
  $this->load->view('loan_menu');

  $data['loan']=$this->Loan_user_m->geteditdata($id);	

  $this->load->view('loan_user/edit_loandetails',$data);

}

function editloan(){
  $params=array();

  $id=$this->input->post('id');

  $params['FatherName']=$this->input->post('fathername');
  $params['GrandFatherName']=$this->input->post('grandfathername');
  $params['Purpose']=$this->input->post('purpose');
  $params['MonthlyExpense']=$this->input->post('monthlyexpense');
  $params['MOnthlyIncome']=$this->input->post('monthlyincome');

  $params['AppliedLoan']=$this->input->post('appliedloan');
  $params['user_id']=$this->session->userdata('id');

  $Collateral=$this->input->post('collateral');
  for($i=0;$i<count($Collateral);$i++){
   $data_c[]= $Collateral[$i];
 }
 $params['Collateral']=implode(",",$data_c);

 $params['LoanApplied']='0';

 $data=$this->Loan_user_m->updateloan($id,$params);
 if ($data){
  $this->session->set_flashdata('success','Please recheck to apply loan');
  redirect('loan_user/appiledload');
}
else{

}
}

function changephoto(){
         if(!$this->session->userdata('login')){
    redirect('Login');

  }
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');
}
    $this->load->view('header');

  $this->load->view('loan_menu');
  $this->load->view('loan_user/changephoto');


}

function updatephoto(){
      if(!$this->session->userdata('login')){
        redirect('Login');
      }
      $id=$this->session->userdata('id');




      $config['upload_path'] = './uploads/loan';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['overwrite']    =  TRUE;

        //$config['encrypt_name'] = true;

      $config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
        // $config['max_height']  = '720';   
      $config['overwrite']    =  TRUE;
      $config['file_name']  = uniqid(rand());  
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      $type= explode('.',$_FILES['image']['name']);
      $type= $type[count($type)-1];

      $params['photo'] = $config['file_name'].'.'.$type;


      $upload = $this->upload->data();


      if(!$this->upload->do_upload('image'))

      {


        $error = array('error' => $this->upload->display_errors());              
        $this->session->set_flashdata('error', $this->upload->display_errors());  
      }
      else{

        $getimage=$this->Common_m->getphoto($id);

        foreach($getimage as $i){
          unlink ("./uploads/loan/".$i->photo );
        }
        $data=$this->Common_m->update($id,$params,'cooperative_list');


        if($data){


          $session_array = array(
            'photo'=>$params['photo']);
          $this->session->set_userdata($session_array);
          $this->session->set_flashdata('success','Successfully photo changed');
          redirect('Login/login_success');
        }
        else{
          $this->session->set_flashdata('error','Server error please try again !');
          redirect('Login/login_success');
        }

      }

    }

    function editprofile(){
      if(!$this->session->userdata('login')){
  redirect('Login');
}
    $usertype=$this->session->userdata('usertype');
if($usertype!='Loan'){
  redirect('Login');

}

$id=$this->session->userdata('id');
$this->load->view('header');
$this->load->view('loan_menu');
$data['loan']=$this->Loan_user_m->getprofile($id);
$this->load->view('loan_user/edit_profile',$data);
    }


     function updateprofile(){
      $id=$this->session->userdata('id');





        $data['name'] = $this->input->post('name');
  $data['address'] = $this->input->post('address');
  $params['Gender'] = $this->input->post('gender');
  $params['DOB'] = $this->input->post('dob');


  $params['Occupation'] = $this->input->post('occupation');

  $contact=$this->input->post('contact');
  $education=$this->input->post('education');


  for($i=0;$i<count($contact);$i++){
    $data_c[]= $contact[$i];
  }
  $data['contact_no']=implode(",",$data_c);
      //echo $allcontact;

      //education
  
  for($i=0;$i<count($education);$i++){
    $data_e[]= $education[$i];
  }
  $params['Education']=implode(",",$data_e);



$update=$this->Loan_user_m->updateprofile($id,$data,$params);
if($update){
      $this->session->set_flashdata('success','Server error please try again !');
          redirect('Loan_user/viewprofile'); 
}
     }

}
?>