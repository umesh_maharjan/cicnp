<br>
<H3 style="text-align:center"> Customer List </H3>
<div class="container">

	<?php 
	if($this->session->flashdata('success')){
		echo '<div class="alert alert-success">';
		echo $this->session->flashdata('success');
		echo '</div>';
	}

	?>
	<?php 
	if($this->session->flashdata('error')){
		echo '<div class="alert alert-danger">';
		echo $this->session->flashdata('error');
		echo '</div>';
	}

	?>
	<div class="row" style="align:center;">
		
		<div class="col-md-12"><form action="<?php echo base_url('Customer/searchlist');?>" method="post">
			<table>
				<td class="col-md-4" >
					<td class="col-md-3" ><select class="form-control"  name="search">

						<option value="0">--Select type--</option>
						<option value="1">Organization Name</option>
						<option value="2">Customer Name</option>
					</select>
				</td>

				<td class="col-md-3" > <input type="text" name="search_item"  class="form-control" placeholder="Search" required></td>
				<td> <input type="submit"   value="Search" class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;"></td>
			</table>
		</form>

	</div></div>

	<div class="row">
		
		<div class="col-md-12" style="text-align:right"> <a href="<?php echo base_url();?>Customer/addcustomerview" class="btn btn-success btn-md" ><i class="fa fa-plus"></i>Add Customer</a></div>
		
	</div>
	<table class=" table table-bordered table-hover table-responsive" style="margin-top:20px;">
		<tr>
			<th>S.N</th>
			<th>Organization Name</th>

			<th>Customer Name</th>
			<th>Address</th>
			<th>Contact Number</th>
			

		
		</tr>
		<?php 

		if(!empty($customer)){
			$sn = 1;
			foreach($customer as $r){ 


?>
			<tr> 

				<th> <?php echo $sn?>
				</th>
				

				<th> <?php echo ucwords($r->name);?>
				</th>
				<th> <?php echo $r->customer_name;?>
				</th>
				

				<th> <?php echo ucwords($r->address);?>
				</th>
				<th> <?php echo ucwords($r->contact);?>
				</th>

			

			</tr>
			<?php  $sn++;} } else {
				?>
			</table>
			<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
				There is no  Customer</div>'

				<?php } ?>

			</div>
			<script type="text/javascript">
				document.getElementById('nav_user').className += "active";
			</script>
