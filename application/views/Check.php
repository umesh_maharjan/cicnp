<!DOCTYPE html>
<html>
<head>

<style type="text/css">
   @font-face
   {
    font-family: 'PCSNEWN'; /*a name to be used later*/
    src: url('<?php echo base_url(); ?>assets/PCSNEWN.TTF');
  }

   td{
    
      font-family: 'PCSNEWN';
  }
  </style>
 
	<title>Test</title>
</head>
<body>

<nav class="navbar navbar-inverse" style="background-color:#558C89;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
   
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
       
<a class="navbar-brand" href="<?php echo base_url();?>" style="color:#FFF;font-size:28px;">CICNP</a>
      </ul>
      <ul class="nav navbar-nav navbar-right">
       <li id="home"><a class="nav-a" href="<?php echo base_url();?>">Home</a></li>
      </ul>
    
</nav>
<div class="container">
<h3>Cooperative List</h3>
<table class="table table-bordered">
	<tr>
		<th>S.No</th>
		<th>DartaNo</th>
		<th>DartaMiti</th>
		<th>Name</th>
		<th>Address</th>
		<th>Type</th>
	</tr>
	<?php 
		foreach($value as $r){
			echo "<tr>";
			echo "<td>".$r->Id."</td>";
			echo "<td>".$r->DartaNo."</td>";
			echo "<td>".$r->DartaMiti."</td>";
			echo "<td>".$r->Name."</td>";
			echo "<td>".$r->Address."</td>";
			//echo "<td>".$r->Workplace."</td>";
			echo "<td>".$r->Type."</td>";
			//echo "<td>".$r->ShareMember."</td>";
			echo "</tr>";
		}
	 ?>
</table>
</div>
</body>
</html>