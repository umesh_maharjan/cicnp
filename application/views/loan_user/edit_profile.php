<br>



<?php foreach($loan as $r){
    $name=$r->name;
    $address=$r->address;
    $contact=$r->contact_no;
    $education=$r->Education;
    $dob=$r->DOB;
    $gender=$r->Gender;
    $occupation=$r->Occupation;
    }

$myArray = explode(',', $contact); 
$educationarray=explode(',', $education); 
?>

<style>
  #nav_setting .dropdown-toggle
{
    background-color:#74AFAD;

  color:white !important;
}
</style>




<div class="container">
<h3 style="text-align:center">Edit Organization Profile Form</h3>

<form id="add_user" method="post" action="<?php echo base_url('Loan_user/updateprofile'); ?>" class="form-horizontal"  enctype="multipart/form-data">

<?php 

if($this->session->flashdata('success')){
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('success');
    echo '</div>';
}
if($this->session->flashdata('error')){
    echo '<div class="alert alert-danger">';
    echo $this->session->flashdata('error');
    echo '</div>';
}
?>

    <div class="form-group">

    <label for="firstname" class="col-sm-2 control-label"> Name</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="name" id="name"  value="<?php echo $name;?>" style="text-transform: capitalize;" required/>
    </div>  
    </div>

    <div class="form-group">
    <label for="middlename" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="address" value="<?php echo $address;?>"style="text-transform: capitalize;" id="address" required />
    </div>
    </div>

     <div class="form-group">
    <label for="middlename" class="col-sm-2 control-label">Date of Birth</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" name="dob" value="<?php echo $dob;?>"style="text-transform: capitalize;" id="address" required />
    </div>
    </div>
        <div class="form-group">
    <label for="contact" class="col-sm-2 control-label"> Contact Number</label>
    <div class="col-sm-7">
    <?php 
    $ca=0;
    foreach($myArray as $a) {
       $ca=1;
    

echo "<div id='editcontact".$ca."'><input type='number' class='form-control' value=".$a." name='contact[]' id='contactbod".$ca."'  placeholder='Contact' required/><div class='col-sm-0'><button class='btn btn-default'  onclick='remove_editcont(".$ca.")'>Remove</button></div></div>";
$ca=$ca+1;


     }?>
    </div>
    <div class="col-sm-0">
        <button class="btn btn-default" id="add_more_contact" onclick="addContact()">Add More</button>
    </div>
    </div> 


    <div id="add_contact"></div>     



<div class="form-group">
    <label for="education " class="col-sm-2 control-label"> Education Qualification</label>
    <div class="col-sm-7">

        <?php $edu=0;
        foreach($educationarray as $q){
            $edu=1;


            echo "<div id='editedu".$edu."'><input type='text' class='form-control' value=".$q." name='education[]' id='edu_qlfy".$edu."'  placeholder='Eduacation qualification' required/><div class='col-sm-0'><button class='btn btn-default'  onclick='remove_editedu(".$edu.")'>Remove</button></div></div>";
            $edu=$edu+1;


        }?>    

    </div>
    <div class="col-sm-0">
        <button class="btn btn-default" id="add_more_education" onclick="addeducation()">Add More</button>
    </div>
</div> 
  
<div id="add_education"></div> 

   <div class="form-group">
            <label for="gender" class="col-sm-2 control-label">Gender</label>
            <div class="col-sm-8">

            <?php if($gender=='Male'){ ?>
                <input type="radio" name="gender" id="gender"  value="Male"  checked/>Male
                <input type="radio"  name="gender" id="gender"  value="Female" />Female
                <?php } else { ?>
                 <input type="radio" name="gender" id="gender"  value="Male"  />Male
                <input type="radio"  name="gender" id="gender"  value="Female" checked />Female
                <?php }?>
            </div>  
        </div>



            <div class="form-group">
        <label for="occupation" class="col-sm-2 control-label">Occupation</label>
        <div class="col-sm-8">
            <select id="occupation" name="occupation" class="form-control">
             <option value="<?php echo $occupation;?>"><?php echo $occupation;?></option>
             <option value="Student">Student</option>
             <option value="Self Employed">Self Employed</option>
             <option value="Employee">Employee</option>
             <option value="Other">Other</option>
         </select>
     </div>  
 </div>


        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
</form>
</div>


<script type="text/javascript">
    var count= 0;
    var addContact = function(){
        count = count+1;
        console.log(count);
        $("#add_contact").append("<div class='container' id='contact"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='number' class='form-control' name='contact[]'  style='text-transform: capitalize;' placeholder='Contact' required/></div><div class='col-sm-0'><button class='btn btn-default' id='contact"+count+"' onclick='removeContact("+count+")'>Remove</button></div></div>");
    };

    var removeContact = function(c){
        console.log("remove count: "+c);
        console.log("contact"+c);
        $("#contact"+c).remove();
        
    };



    var remove_editcont= function(ed){
        // console.log("remove countedu: "+ed);
        // console.log("education"+ed);
        $("#editcontact"+ed).remove();
        
    };

    var remove_editedu= function(ed){
        // console.log("remove countedu: "+ed);
        // console.log("education"+ed);
        $("#editedu"+ed).remove();
        
    };


    var countedu= 0;
    var addeducation = function(){
        countedu = countedu+1;
        console.log(countedu);
        $("#add_education").append("<div class='container' id='education"+countedu+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' class='form-control' name='education[]'  style='text-transform: capitalize;' placeholder='Education qualification' required/></div><div class='col-sm-0'><button class='btn btn-default' id='education"+countedu+"' onclick='remove_education("+countedu+")'>Remove</button></div></div>");
    };

    var remove_education= function(ed){
        console.log("remove countedu: "+ed);
        console.log("education"+ed);
        $("#education"+ed).remove();
        
    };
  


</script>




