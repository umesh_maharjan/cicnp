<br>

<div class="container">
<h3 style="text-align:center">Loan applicant Registration Form</h3>
    <form id="loan_form" method="post" action="<?php echo base_url('loan_user/adduser');?>" enctype="multipart/form-data" class="form-horizontal" >
        <?php 

        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }

        ?>
        <div class="form-group">

            <label for="name" class="col-sm-2 control-label"> Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="name" id="name" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">

            <label for="address" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="address" id="address"  style="text-transform: capitalize;"  required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="gender" class="col-sm-2 control-label">Gender</label>
            <div class="col-sm-8">
                <input type="radio" name="gender" id="gender"  value="Male"  checked/>Male
                <input type="radio"  name="gender" id="gender"  value="Female" />Female
            </div>  
        </div>

        <div class="form-group">
            <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
            <div class="col-sm-8">
                <input type="date" class="form-control" name="dob" id="dob"    required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="contact" class="col-sm-2 control-label">Contact Number</label>
            <div class="col-sm-7">
                <input type="text" pattern='[0-9]{7,10}' class="form-control" placeholder="Contact number should be between 7-10" name="contact[]" id="contact"  style="text-transform: capitalize;"  required/>
            </div>
            <div class="col-sm-0">
             <button class="btn btn-default" id="add_more_contact" onclick="addContact()">Add More</button>
         </div>
     </div>

     <div id="add_contact"></div>

     <div class="form-group">
        <label for="education" class="col-sm-2 control-label">Educational Qualifications</label>
        <div class="col-sm-7">
            <input type="text" class="form-control" name="education[]" id="education"  style="text-transform: capitalize;"  required/>
        </div> 
        <div class="col-sm-0">
            <button class="btn btn-default" id="add_more_education" onclick="addEducation()">Add More</button>
        </div> 
    </div>

    <div id="add_education"></div>

    <div class="form-group">
        <label for="email" class="col-sm-2 control-label"> Valid Email</label>
        <div class="col-sm-8">
            <input type="email" class="form-control" name="email" id="email" placeholder="Please enter valid email"    required/>
        </div>  
    </div>

    <div class="form-group">
        <label for="email" class="col-sm-2 control-label"> Password</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" name="password" id="password"  style="text-transform: capitalize;"  required/>
        </div>  
    </div>

    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Confirm Password</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" name="c_password" id="c_password"  style="text-transform: capitalize;"  required/>
        </div>  
    </div>



    <div class="form-group">

        <label for="citizen number" class="col-sm-2 control-label">Citizen Number</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="citizen" id="citizen"    required/>
        </div>  
    </div>






    <div class="form-group">

        <label for="address" class="col-sm-2 control-label">Citizen Issued Place</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="citizen_place" id="citizen_place"   style="text-transform: capitalize;"  required/>
        </div>  
    </div>



    <div class="form-group">

        <label for="address" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-8">
            <input type="file" class="form-control" name="image" id="image"    required/>
        </div>  
    </div>


    <div class="form-group">
        <label for="occupation" class="col-sm-2 control-label">Occupation</label>
        <div class="col-sm-8">
            <select id="occupation" name="occupation" class="form-control">
             <option value="None">-- Select --</option>
             <option value="Student">Student</option>
             <option value="Self Employed">Self Employed</option>
             <option value="Employee">Employee</option>
             <option value="Other">Other</option>
         </select>
     </div>  
 </div>

 <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />

</form>
</div>

<script type="text/javascript">
	var count= 0;
    var ecount = 0;
    var addContact = function(){
      count = count+1;
		//console.log(count);
		$("#add_contact").append("<div class='container' id='contact"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' pattern='[0-9]{7,10}' class='form-control' name='contact[]'  style='text-transform: capitalize;' placeholder='Contact' required/></div><div class='col-sm-0'><button class='btn btn-default' id='contact"+count+"' onclick='removeContact("+count+")'>Remove</button></div></div>");
	};

	var removeContact = function(c){
		// console.log("remove count: "+c);
		// console.log("contact"+c);
		$("#contact"+c).remove();
		
	};

    var addEducation = function(){
        ecount = ecount+1;
        //console.log(ecount);
        $("#add_education").append("<div class='container' id='education"+ecount+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' class='form-control' name='education[]'  style='text-transform: capitalize;' placeholder='Educational Qualifications' required/></div><div class='col-sm-0'><button class='btn btn-default' id='education"+count+"' onclick='removeEducation("+ecount+")'>Remove</button></div></div>");
    };

    var removeEducation = function(c){
        // console.log("here");
        // console.log("remove count: "+c);
        // console.log("education"+c);
        $("#education"+c).remove();
        
    };
</script>