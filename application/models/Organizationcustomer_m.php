<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organizationcustomer_m extends CI_Model {



function getcustomer($limit,$offset,$org_id){
	$this->db->select('customer.*,cooperative_list.name')
	->from('customer')
	 ->join('cooperative_list','customer.org_id=cooperative_list.org_id')
	->where('customer.org_id',$org_id);

	   $this->db->limit($limit,($offset*$limit));
		$this->db->order_by('customer.created_date','desc');
		return $this->db->get()->result();



}

		function totalcustomer($org_id){
  $this->db->select('id')
  ->from('customer')
  ->where('org_id',$org_id);
  return $this->db->get()->num_rows();
}

	public function getcustomerbyname($name,$org_id){

$where='';
$where.="customer.customer_name LIKE '" .$name."%'";
		$this->db->select('customer.*,cooperative_list.name')->from('customer')
		->join('cooperative_list','cooperative_list.org_id=customer.org_id')
		->where($where)
		->where('customer.org_id',$org_id)
		->order_by('customer.id','desc');
		return $this->db->get()->result();
	}

	function checkcustomer_id($id){
		$this->db->select('id')->from('customer')->where('customer_id',$id);


		return $this->db->get()->result();
	}

	function insert($input){
		return $this->db->insert('customer', $input);
	}

	 function getcustomerbyid($id){
	 	$this->db->select('*')->from('customer')->where('id',$id);
	 	return $this->db->get()->result();

	 }
	function checkcustomeid($id,$customer_id){
		$this->db->select('id')->from('customer')->where('customer_id ',$customer_id);

 $this->db->where_not_in('id', $id);
		return $this->db->get()->result();
	}

public function update($id,$input)

	{

		return $this->db->where('id', $id)->update('customer', $input);
	}

	 function deletecustomer($id){

	return
		$this->db->where('id',$id)
		->delete('customer');
	 }
function totalbalance($org_id)
{
	$this->db->select('total_balance,persms')->from('add_balance')->where('org_id',$org_id);
	return $this->db->get()->result();
}

public function updatebalance($id,$input)

	{

		return $this->db->where('org_id', $id)->update('add_balance', $input);
	}
	 public function smslog($input){
	 	return $this->db->insert('sms_log', $input);
	 }

}

