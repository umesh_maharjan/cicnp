<br>
<H3 style="text-align:center"> Loan applied list </H3>
<div class="container">

	<?php 
	if($this->session->flashdata('success')){
		echo '<div class="alert alert-success">';
		echo $this->session->flashdata('success');
		echo '</div>';
	}
	if($this->session->flashdata('error')){
		echo '<div class="alert alert-danger">';
		echo $this->session->flashdata('error');
		echo '</div>';
	}


	?>

	<table class=" table table-bordered table-hover table-responsive">
		<tr>
			<th>S.N</th>
			<th> Father Name</th>
			<th> Grand Father Name</th>
			<th> Purposed </th>
			<th>  Monthly Expenses </th>
			<th> Monthly Income </th>
			<th> Applied Income </th>
			<th>Collateral</th>
			<th>Status</th>
			<th>Action</th>
			
			
			
		</tr>

		
		<?php 

		if(!empty($loan)){
			$sn = 1;
			foreach($loan as $r){ 

				$Collateralarray = explode(',', $r->Collateral);?>
			<tr> 

				<td style="padding:6px"> <?php echo $sn?>
				</td>
				<td style="padding:6px"> <?php echo ucwords($r->FatherName);?>
				</td>


				<td style="padding:6px"> <?php echo ucwords($r->GrandFatherName);?>
				</td>
				<td style="padding:6px"> <?php echo $r->Purpose;?>
				</td>
				<td style="padding:6px"> <?php echo $r->MonthlyExpense;?>
				</td>
				<td style="padding:6px"> <?php echo $r->MonthlyIncome;?>
				</td>
				<td style="padding:6px"> <?php echo $r->AppliedLoan;?>
				</td>
					<td> 
						<?php foreach($Collateralarray as $c){
							?>
							<li>
								<?php echo ucwords($c);?></li>
								<?php 
							}?>

						</td>




				<?php if($r->LoanApplied=="1"){?>
				<td style="padding:6px"> Loan Applied
				</td>
				<?php } else { ?>
				<td style="padding:6px"> Not Applied
				</td> <?php } ?>
				<?php if($r->LoanApplied=="0"){?>

				<td style="padding:6px"><a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php echo base_url();?>loan_user/editloanlist?id=<?php echo $r->id;?>">Edit</a> 
					

<a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php echo base_url();?>loan_user/applied?id=<?php echo $r->id;?>">Apply</a> 
					<a class="btn btn-danger btn-md"style="padding:8px 20px 8px 20px;margin-top:5px" href="<?php echo base_url();?>loan_user/applied?id=<?php echo $r->id;?>">Delete</a> 
					</td>
					<?php } else {?>

					<td><a class="btn btn-danger btn-md"style="padding:8px 20px 8px 20px;" href="<?php echo base_url();?>loan_user/deleteloan?id=<?php echo $r->id;?>">Delete</a> </td>
				</td>
				

				<?php } ?>	
			</tr>
			
			<?php $sn++; } } else {
				?>
				
	
			</table>
			<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
There is no Loan list</div>
<?php } ?>



		</div>





		<script type="text/javascript">
			document.getElementById('nav_user').className += "active";
		</script>
