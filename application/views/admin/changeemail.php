<br>
<div class="container">
<h3 style="text-align:center">Email Change Form</h3>
    <form id="add_user" method="post" action="<?php echo base_url('login/updateemail'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>
        
        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> New Email</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" name="email" id="email" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="middlename" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" name="password"  id="password" required />
            </div>
        </div>
        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
    </form>
</div>
