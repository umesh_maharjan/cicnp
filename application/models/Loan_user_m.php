<?php 
class Loan_user_m extends CI_Model{
	public function adduser($data,$params){

		$this->db->insert('cooperative_list',$data);
		$params['user_id'] = $this->db->insert_id();

		
		return $this->db->insert('loan_user_details', $params);
		
		
	}



	public function checkemail($email){
		$this->db->select('Email')
		->from('cooperative_list')
		->where('email',$email);
		return $this->db->get()->result();
	}


	public function checkcitizen($citizen){
		$this->db->select('CitizenshipNo')
		->from('loan_user_details')
		->where('CitizenshipNo',$citizen);
		return $this->db->get()->result();
	}


	


	function getuserinfo($email,$password){
		$this->db->select('8')
		->from('loan_user_details')
		->where('Email',$email)
		->where('Password',$password);
		return $this->db->get()->result();
	}

		   //active account
	function active_account($email,$input){

		return $this->db->where('Email', $email)->update('loan_user_details', $input);
	}
// insert applied load
	function applyloan($input){
		return $this->db->insert('loan_apply', $input);
	}

//get loan apllied list

	function loanlist($id){
		$this->db->select('*')
		->from('loan_apply')
		->where('user_id',$id)
		->where('archive','0')
		->order_by('id','desc');


		return $this->db->get()->result();
	}

	function updateloanapplied($id,$params){

		return $this->db->where('id', $id)->update('loan_apply', $params);

	}

	function getloanlist($limit,$offset){
		$this->db->select('loan_apply.*,cooperative_list.name,notification.org_id,notification.view',false)
		->from('loan_apply')
		->join('cooperative_list','cooperative_list.org_id=loan_apply.user_id')
		->join('notification','notification.loan_id=loan_apply.id','left')
		->where('loan_apply.Loanapplied','1')
		->order_by('loan_apply.created_on','desc');

		$this->db->limit($limit,($offset*$limit));
		return $this->db->get()->result();
	}





	function getnotificationdetail($id){
		$this->db->select('loan_apply.*,cooperative_list.name,cooperative_list.contact_no,cooperative_list.email,cooperative_list.address,loan_user_details.Gender,loan_user_details.DOB,
			loan_user_details.Occupation,loan_user_details.Education,loan_user_details.CitizenshipNo,loan_user_details.CitizenshipIssued')
		->from('loan_apply')
		->join('cooperative_list','cooperative_list.org_id=loan_apply.user_id')
		->join('loan_user_details','cooperative_list.org_id=loan_user_details.user_id')
		->where('loan_apply.id',$id);
		return $this->db->get()->result();

	}

	function checknotification($org_id,$loan_id){
		$this->db->select('id')->from('notification')
		->where('org_id',$org_id)
		->where('loan_id',$loan_id);
		return $this->db->get()->result();	
	}

	function updatenotification($params){
		return $this->db->insert('notification', $params);
	}

	function   deleteupdate($id,$params){


		return $this->db->where('id', $id)->update('loan_apply', $params);
	}

	function   deleteupdatenotify($id,$params){


		return $this->db->where('loan_id', $id)->update('notification', $params);
	}

	function getprofile($id){
		$this->db->select('cooperative_list.name,cooperative_list.address,cooperative_list.contact_no,cooperative_list.email,
			loan_user_details.Gender,loan_user_details.DOB,
			loan_user_details.Occupation,loan_user_details.Education,loan_user_details.CitizenshipNo,loan_user_details.CitizenshipIssued')
		->from('cooperative_list')
		->join('loan_user_details','cooperative_list.org_id=loan_user_details.user_id')
		->where('cooperative_list.org_id',$id);
		return $this->db->get()->result();


	}

	function geteditdata($id){
		$this->db->select('*')->from('loan_apply')
		->where('id',$id);
		return $this->db->get()->result();

	}

	function   updateloan($id,$params){


		return $this->db->where('id', $id)->update('loan_apply', $params);
	}

		function   updateprofile($id,$data,$params){

 $this->db->where('org_id', $id)->update('cooperative_list', $data);
				return $this->db->where('user_id', $id)->update('loan_user_details', $params);
	}




}

?>