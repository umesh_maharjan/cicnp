<?php if(empty($email)){?>
<script>
  window.location.href = "index";
  </script>
<?php } else { ?>
<script type="text/javascript">
  $(window).load(function(){
    $('#myModal').modal('show');
  });
</script>
<?php }?>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h3 align="center"> Enter New Password</h3>
      </div>

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>
 <div class="modal-body">
      <form action="<?php echo base_url();?>Login/reset_password" method="post">

     <div class="form-group">
    <label for="password" class="col-sm-4 control-label">New Password</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" name="new_password" id="new_password" required/>
    </div>
    </div>
<br><br>
  <div class="form-group">
    <label for="comfirm password" class="col-sm-4 control-label">Confirm Password</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" name="c_password" id="c_password" required/>
    </div>
    </div>
    <input type="hidden" name="email" value="<?php echo $email;?>">


</div>
      <div class="modal-footer">
       <input type="submit" class="btn btn-success" Value="Submit">
        <button type="button" class="btn btn-success" onclick="reload()">Cancel</button>
      </div>

      </form>
    

  </div>
</div>
<script type="text/javascript">

	var reload = function(){
		window.location.href = "index";
	}
</script>


