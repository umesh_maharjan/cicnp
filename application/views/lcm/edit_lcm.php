<br>

<?php foreach($bod as $r){
    $name=$r->name;
    $address=$r->address;
    $contact=$r->contact;
    $gender=$r->gender;
    $image=$r->photo;
    $education=$r->edu_qulfy;
    $id=$r->id;



}

$myArray = explode(',', $contact);
$edu_array = explode(',', $education);

?>

<style>
  #nav_content .dropdown-toggle
{
    background-color:#74AFAD;

  color:white !important;
}
</style>

<div class="container">

<h3 style="text-align:center">Edit LCM Form</h3>

    <form id="add_user" method="post" action="<?php echo base_url('management/editbod'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="name" id="name" value="<?php echo $name;?>"style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="middlename" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="address" style="text-transform: capitalize;" value="<?php echo $address;?>" id="address" required />
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Gender</label>


            <?php if($gender=='Male'){ ?>
            <div class="col-sm-8">
                <input type="radio"  name="gender" id="gender"  value="Male" checked/> Male
                <input type="radio"  name="gender" id="gender"  value="FeMale"/>FeMale
            </div>
            <?php }  else { ?>

            <div class="col-sm-8">
                <input type="radio"  name="gender" id="gender"  value="Male" /> Male
                <input type="radio"  name="gender" id="gender"  value="FeMale" checked/>FeMale
            </div>
            <?php }?>
        </div> 
        <div class="form-group">
            <label for="contact" class="col-sm-2 control-label"> Contact Number</label>
            <div class="col-sm-7">
                <?php 
                $ca=0;
                foreach($myArray as $a) {
                 $ca=1;
                 

                 echo "<div id='editcontact".$ca."'><input type='number' class='form-control' value=".$a." name='contact[]' id='contactbod".$ca."'  placeholder='Contact' required/><div class='col-sm-0'><button class='btn btn-default'  onclick='remove_editcont(".$ca.")'>Remove</button></div></div>";
                 $ca=$ca+1;


             }?>
         </div>
         <div class="col-sm-0">
            <button class="btn btn-default" id="add_more_contact" onclick="addContact()">Add More</button>
        </div>
    </div> 


    <div id="add_contact"></div>     


    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-8">
            <input type="file" class="form-control" name="image" id="image" />
        </div>
        
    </div>

    <div class="form-group">
       <label for="email" class="col-sm-2 control-label"></label>
       <div class="col-sm-2">

        <img src="<?php echo base_url();?>uploads/lcm/<?php echo $image;?>" width=100 height=100>

    </div>

</div>


<div class="form-group">
    <label for="education " class="col-sm-2 control-label"> Education Qualification</label>
    <div class="col-sm-7">

        <?php $edu=0;
        foreach($edu_array as $q){
            $edu=1;


            echo "<div id='editedu".$edu."'><input type='text' class='form-control' value=".$q." name='education[]' id='edu_qlfy".$edu."'  placeholder='Eduacation qualification' required/><div class='col-sm-0'><button class='btn btn-default'  onclick='remove_editedu(".$edu.")'>Remove</button></div></div>";
            $edu=$edu+1;


        }?>    

    </div>
    <div class="col-sm-0">
        <button class="btn btn-default" id="add_more_education" onclick="addeducation()">Add More</button>
    </div>
</div> 


<div id="add_education"></div> 



<input type="hidden" name="type" id="type" value="LCM">
<input type="hidden" name="id" id="id" value="<?php echo $id;?>">



<input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
</form>
</div>


<script type="text/javascript">
    var count= 0;
    var addContact = function(){
        count = count+1;
        console.log(count);
        $("#add_contact").append("<div class='container' id='contact"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='number' class='form-control' name='contact[]'  style='text-transform: capitalize;' placeholder='Contact' required/></div><div class='col-sm-0'><button class='btn btn-default' id='contact"+count+"' onclick='removeContact("+count+")'>Remove</button></div></div>");
    };

    var removeContact = function(c){
        console.log("remove count: "+c);
        console.log("contact"+c);
        $("#contact"+c).remove();
        
    };



    var remove_editcont= function(ed){
        // console.log("remove countedu: "+ed);
        // console.log("education"+ed);
        $("#editcontact"+ed).remove();
        
    };
    var remove_editedu= function(ed){
        // console.log("remove countedu: "+ed);
        // console.log("education"+ed);
        $("#editedu"+ed).remove();
        
    };


    var countedu= 0;
    var addeducation = function(){
        countedu = countedu+1;
        console.log(countedu);
        $("#add_education").append("<div class='container' id='education"+countedu+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' class='form-control' name='education[]'  style='text-transform: capitalize;' placeholder='Education qualification' required/></div><div class='col-sm-0'><button class='btn btn-default' id='education"+countedu+"' onclick='remove_education("+countedu+")'>Remove</button></div></div>");
    };

    var remove_education= function(ed){
        console.log("remove countedu: "+ed);
        console.log("education"+ed);
        $("#education"+ed).remove();
        
    };


</script>
