<br>
<div class="container">
<h3 style="text-align:center">Organization Registration Form</h3>

    <form id="add_user" method="post" action="<?php echo base_url('Login/adduser'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="name" id="name" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="middlename" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="address" style="text-transform: capitalize;" id="address" required />
            </div>
        </div>


        <div class="form-group">
            <label for="middlename" class="col-sm-2 control-label">Contact Person</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="cont_person" style="text-transform: capitalize;" id="cont_person" required />
            </div>
        </div>


        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" name="email" id="email" required/>
            </div>
        </div>

        <div class="form-group">
            <label for="lastname" class="col-sm-2 control-label">Registrartin no.</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" name="rege_no" id="rege_no"  required/>
            </div>
        </div>
        <div class="form-group">
            <label for="lastname" class="col-sm-2 control-label">Registrartin Date</label>
            <div class="col-sm-8">
                <input type="date" class="form-control" name="rege_date" id="rege_date"  required/>
            </div>
        </div>


        <div class="form-group">
            <label for="middlename" class="col-sm-2 control-label">Purpose</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="purpose"  id="purpose" required />
            </div>
        </div>
        
        
        
        
        <div class="form-group">
            <label for="contact" class="col-sm-2 control-label"> Offical Contact Number</label>
            <div class="col-sm-7">
                <input type="text"  pattern='[0-9]{7,10}' class="form-control" name="contact[]" placeholder="Contact number should be between 7-10" id="contact"  style="text-transform: capitalize;"  required/>
            </div>
            <div class="col-sm-0">
                <button class="btn btn-default" id="add_more_contact" onclick="addContact()">Add More</button>
            </div>
        </div> 
        <div id="add_contact"></div>     


        <div class="form-group">
            <label for="email" class="col-sm-2 control-label"> Password</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" name="password" id="password"  style="text-transform: capitalize;"  required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Confirm Password</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" name="c_password" id="c_password"  style="text-transform: capitalize;"  required/>
            </div>  
        </div>

         <div class="form-group">
            <label for="middlename" class="col-sm-2 control-label">Photo</label>
            <div class="col-sm-8">
                <input type="file" class="form-control" name="image"  id="image" required />
            </div>
        </div>

        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
    </form>
</div>


<script type="text/javascript">
    var count= 0;
    var addContact = function(){
        count = count+1;
        console.log(count);
        $("#add_contact").append("<div class='container' id='contact"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' pattern='[0-9]{7,10}' class='form-control' name='contact[]'  style='text-transform: capitalize;' placeholder='Contact' required/></div><div class='col-sm-0'><button class='btn btn-default' id='contact"+count+"' onclick='removeContact("+count+")'>Remove</button></div></div>");
    };

    var removeContact = function(c){
        console.log("remove count: "+c);
        console.log("contact"+c);
        $("#contact"+c).remove();
        
    };
</script>
