<?php 
class Management_m extends CI_model{


	function getbod($id){
		$this->db->select('member.*,cooperative_list.name as org_name')
		->from('member')
		->join('cooperative_list','member.organization_id=cooperative_list.org_id')
		->where('member.organization_id',$id)
		->where('type','BOD')
		->order_by('id','desc');
		return $this->db->get()->result();
	}

	function insert($input){
		return $this->db->insert('member', $input);
	}




	public function update($id,$input)

	{

		return $this->db->where('id', $id)->update('member', $input);
	}



	function getbod_id($id){
		$this->db->select('*')
		->from('member')
		->where('id',$id);

		return $this->db->get()->result();
	}

	function getlcm($id){
		$this->db->select('member.*,cooperative_list.name as org_name')
		->from('member')
		->join('cooperative_list','member.organization_id=cooperative_list.org_id')
		->where('member.organization_id',$id)
		->where('type','LCM')
		->order_by('id','desc');
		return $this->db->get()->result();
	}

 	//get photo to edit

	function getphoto($id){
		$this->db->select('photo')
		->from('member')
		->where('id',$id);
		return $this->db->get()->result();

	}

 	//get total loan count

	function getloancount(){
		$this->db->select('id')
		->from('loan_apply')
		->where('LoanApplied','1');
		return $this->db->get()->num_rows();
	}

	function getnotificationcount($id){
		$this->db->select('id')	
		->from('notification')
		->where('org_id',$id)
		->where('archive_loan','0');
		return $this->db->get()->num_rows();
	}

//delet user
	public function deleteuser($id){
		return
		$this->db->where('id',$id)
		->delete('member');
	}

 //otm

	function getotmlist($id){
		$this->db->select('*')
		->from('otm')
		->where('org_id',$id);

		return $this->db->get()->result();
	}


	function insertomt($input){
		return $this->db->insert('otm', $input);
	}

	function getomtid($id){
		$this->db->select('*')
		->from('otm')
		->where('id',$id);

		return $this->db->get()->result();
	}

	function updateomt($id,$params){
		return $this->db->where('id', $id)->update('otm', $params);
	}

	public function deleteomt($id){
		return
		$this->db->where('id',$id)
		->delete('otm');
	}
 function checkreg($id,$reg_no){
 	$this->db->select('org_id')
 	->from('cooperative_list')
 	->where('reg_no',$reg_no)
 	->where('org_id !=',$id);
 	return  $this->db->get()->result();
 }
 	public function updateprofile($id,$input)

	{

		return $this->db->where('org_id', $id)->update('cooperative_list', $input);
	}



	public function searchterm_handler($searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata('search_type', $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata('search_type'))
		{
			$searchterm = $this->session->userdata('search_type');
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}

	public function searchvalue_handler($searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata('search_value', $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata('search_value'))
		{
			$searchterm = $this->session->userdata('search_value');
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}

}

?>