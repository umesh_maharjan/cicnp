
<style>
  #nav_content .dropdown-toggle
{
 background-color:#74AFAD;

  color:white !important;
}
</style>
<br>
<div class="container">
<h3 style="text-align:center">Add BOD Form</h3>
    <form id="add_user" method="post" action="<?php echo base_url('management/addbod'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="name" id="name" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="middlename" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="address" style="text-transform: capitalize;" id="address" required />
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Gender</label>
            <div class="col-sm-8">
                <input type="radio"  name="gender" id="gender"  value="Male" checked/> Male
                <input type="radio"  name="gender" id="gender"  value="FeMale"/>Female
            </div>
        </div>


        
        
        
        
        
        <div class="form-group">
            <label for="contact" class="col-sm-2 control-label"> Contact Number</label>
            <div class="col-sm-7">
                <input type="text" pattern="[0-9]{7,10}" class="form-control" name="contact[]" id="contact"  required/>
            </div>
            <div class="col-sm-0">
                <button class="btn btn-default" id="add_more_contact" onclick="addContact()">Add More</button>
            </div>
        </div> 


        <div id="add_contact"></div>     


        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Photo</label>
            <div class="col-sm-8">
                <input type="file" class="form-control" name="image" id="image" required/>
            </div>
        </div>





        <div class="form-group">
            <label for="education " class="col-sm-2 control-label"> Education Qualification</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="education[]" id="education"  style="text-transform: capitalize;"  required/>
            </div>
            <div class="col-sm-0">
                <button class="btn btn-default" id="add_more_education" onclick="addeducation()">Add More</button>
            </div>
        </div> 

        
        <div id="add_education"></div> 



        <input type="hidden" name="type" id="type" value="BOD">



        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
    </form>
</div>


<script type="text/javascript">
    var count= 0;
    var addContact = function(){
        count = count+1;
        console.log(count);
        $("#add_contact").append("<div class='container' id='contact"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' pattern='[0-9]{7,10}' class='form-control' name='contact[]'   placeholder='Contact'    required/></div><div class='col-sm-0'><button class='btn btn-default' id='contact"+count+"' onclick='removeContact("+count+")'>Remove</button></div></div>");
    };

    var removeContact = function(c){
        console.log("remove count: "+c);
        console.log("contact"+c);
        $("#contact"+c).remove();
        
    };



    var countedu= 0;
    var addeducation = function(){
        countedu = countedu+1;
        console.log(countedu);
        $("#add_education").append("<div class='container' id='education"+countedu+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' class='form-control' name='education[]'  style='text-transform: capitalize;' placeholder='Education qualification' required/></div><div class='col-sm-0'><button class='btn btn-default' id='education"+countedu+"' onclick='remove_education("+countedu+")'>Remove</button></div></div>");
    };

    var remove_education= function(ed){
        console.log("remove countedu: "+ed);
        console.log("education"+ed);
        $("#education"+ed).remove();
        
    };



   
</script>

