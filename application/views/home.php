<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>CICNP</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="<?php echo site_url('assets/img/new/reckon.png'); ?>" type="image/x-icon" />
    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="./assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/one.style.css">

    <!-- CSS Footer -->
    <link rel="stylesheet" href="./assets/css/footers/footer-v7.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="./assets/plugins/animate.css">    
    <link rel="stylesheet" href="./assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="./assets/plugins/font-awesome/css/font-awesome.min.css">    
    <link rel="stylesheet" href="./assets/plugins/pace/pace-flash.css">
    <link rel="stylesheet" href="./assets/plugins/owl-carousel/owl.carousel.css">    
    <link rel="stylesheet" href="./assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">    
    <link rel="stylesheet" href="./assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">    
    <link rel="stylesheet" href="./assets/plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
    <!--[if lt IE 9]><link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->
    
    <!-- CSS Customization -->
    <link rel="stylesheet" href="./assets/css/custom.css">
</head>

<!-- 
The #page-top ID is part of the scrolling feature.
The data-spy and data-target are part of the built-in Bootstrap scrollspy function.
-->
<body id="body" data-spy="scroll" data-target=".one-page-header" class="demo-lightbox-gallery">
    <!--=== Header ===-->    
    <nav class="one-page-header navbar navbar-default navbar-fixed-top" role="navigation" style="background-color: #FFFFFF">
        <div class="container">
            <div class="menu-container page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="#intro">
                    <!-- <span style="color:#558C89 !important">C</span>ICNP -->
                    <img src="assets/img/new/reckon.png" alt="Logo" width="90px" style="margin-top:-39%;">
                </a>
            </div>
 
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <div class="menu-container">
                    <ul class="nav navbar-nav">
                        <li class="page-scroll home">
                            <a href="#body" style="font-size:14px">Home</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#about" style="font-size:14px">About Us</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#services" style="font-size:14px">Services</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#testimonials" style="font-size:14px">Testimonials</a>
                        </li>
                        <!-- <li class="page-scroll">
                            <a href="#portfolio">Portfolio</a>
                        </li> -->
                        <li class="page-scroll">
                            <a href="#contact" style="font-size:14px">Contact</a>
                        </li>   
                        <li class="page-scroll">
                            <a href="<?php echo site_url('Check/cooperative_list'); ?>">Cooperatives</a>
                        </li>                  
                        <li class="page-scroll">
                            <a href="<?php echo base_url('Login'); ?>" style="font-size:14px">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!--=== End Header ===-->    

    <!-- Intro Section -->
    <section id="intro" class="intro-section">
        <div class="fullscreenbanner-container">
            <div class="fullscreenbanner">
                <ul>
                    <!-- SLIDE 1 -->
                    <li data-transition="curtain-1" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                        <!-- MAIN IMAGE -->
                        <img src="./assets/img/sliders/revolution/bg1.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                        <!-- LAYERS -->
                        <div class="tp-caption rs-caption-1 sft start"
                            data-x="center"
                            data-hoffset="0"
                            data-y="100"
                            data-speed="800"
                            data-start="2000"
                            data-easing="Back.easeInOut"
                            data-endspeed="300">
                            <span style="font-size:38px;">WE ARE CICNP</span>
                        </div>

                        <!-- LAYER -->
                        <div class="tp-caption rs-caption-2 sft"
                            data-x="center"
                            data-hoffset="0"
                            data-y="200"
                            data-speed="1000"
                            data-start="3000"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="off"
                            style="z-index: 6">
                            <!-- Creative freedom matters user experience.<br> -->
                            We minimize the gap between lender and borrower.
                        </div>

                        <!-- LAYER -->
                        <div class="tp-caption rs-caption-3 sft"
                            data-x="center"
                            data-hoffset="0"
                            data-y="360"
                            data-speed="800"
                            data-start="3500"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="off"
                            style="z-index: 6">
                            <span class="page-scroll"><a href="#about" class="btn-u btn-brd btn-brd-hover btn-u-light">Learn More</a></span>
                            <span class="page-scroll"><a href="<?php echo base_url('Login'); ?>" class="btn-u btn-brd btn-brd-hover btn-u-light">Login</a></span>
                        </div>
                    </li>

                    <!-- SLIDE 2 -->
                    <li data-transition="slideup" data-slotamount="5" data-masterspeed="1000" data-title="Slide 2">
                        <!-- MAIN IMAGE -->
                        <img src="./assets/img/sliders/revolution/bg2.jpg" alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                        <!-- LAYERS -->
                        <div class="tp-caption rs-caption-1 sft start"
                            data-x="center"
                            data-hoffset="0"
                            data-y="100"
                            data-speed="800"
                            data-start="1500"
                            data-easing="Back.easeInOut"
                            data-endspeed="300">
                            <span style="font-size:38px;">DEDICATED ADVANCED TEAM</span>
                        </div>

                        <!-- LAYER -->
                        <div class="tp-caption rs-caption-2 sft"
                            data-x="center"
                            data-hoffset="0"
                            data-y="200"
                            data-speed="1000"
                            data-start="2500"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="off"
                            style="z-index: 6">
                            We are creative technology company providing<br> 
                            key digital services.                            
                        </div>

                        <!-- LAYER -->
                        <div class="tp-caption rs-caption-3 sft"
                            data-x="center"
                            data-hoffset="0"
                            data-y="360"
                            data-speed="800"
                            data-start="3500"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="off"
                            style="z-index: 6">
                            <span class="page-scroll"><a href="#about" class="btn-u btn-brd btn-brd-hover btn-u-light">Learn More</a></span>
                            <span class="page-scroll"><a href="<?php echo base_url('Login'); ?>" class="btn-u btn-brd btn-brd-hover btn-u-light">Login</a></span>
                        </div>
                    </li>

                    <!-- SLIDE 3 -->
                    <li data-transition="slideup" data-slotamount="5" data-masterspeed="700"  data-title="Slide 3">
                        <!-- MAIN IMAGE -->
                        <img src="./assets/img/sliders/revolution/bg3.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                        <!-- LAYERS -->
                        <div class="tp-caption rs-caption-1 sft start"
                            data-x="center"
                            data-hoffset="0"
                            data-y="110"
                            data-speed="800"
                            data-start="1500"
                            data-easing="Back.easeInOut"
                            data-endspeed="300">
                            <span style="font-size:38px;">MAINTAIN SOUND CREDIT ENVIRONMENT</span>
                        </div>

                        <!-- LAYER -->
                        <div class="tp-caption rs-caption-2 sfb"
                            data-x="center"
                            data-hoffset="0"
                            data-y="210"
                            data-speed="800"
                            data-start="2500"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="off"
                            style="z-index: 6">
                            CICNP plays crucial role in maintaining transparency in cooperative credit market.
                        </div>

                        <!-- LAYER -->
                        <div class="tp-caption rs-caption-3 sfb"
                            data-x="center"
                            data-hoffset="0"
                            data-y="370"
                            data-speed="800"
                            data-start="3500"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="off"
                            style="z-index: 6">
                            <span class="page-scroll"><a href="#about" class="btn-u btn-brd btn-brd-hover btn-u-light">Learn More</a></span>
                            <span class="page-scroll"><a href="<?php echo base_url('Login'); ?>" class="btn-u btn-brd btn-brd-hover btn-u-light">Login</a></span>
                        </div>
                    </li>                    
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
                <div class="tp-dottedoverlay twoxtwo"></div>
            </div>
        </div>
    </section>
    <!-- End Intro Section -->

    <!--  About Section -->
    <section id="about" class="about-section section-first">
        <div class="block-v1">
            <div class="container">
                <div class="row content-boxes-v3">
                    <div class="col-md-4 md-margin-bottom-30">
                        <i class="icon-custom icon-md rounded-x icon-bg-dark fa fa-lightbulb-o" style="background-color:#558C89;"></i>
                        <div class="content-boxes-in-v3" style="margin-top:1%;">
                            <h2 class="heading-sm" style="color:#e60000;">Easy Activation</h2>
                            <p>Signup and activate your account easily.</p>
                        </div>
                    </div>
                    <div class="col-md-4 md-margin-bottom-30">
                        <i class="icon-custom icon-md rounded-x icon-bg-dark fa fa-flask" style="background-color:#558C89;"></i>
                        <div class="content-boxes-in-v3" style="margin-top:1%;">
                            <h2 class="heading-sm" style="color:#0000b3;">Data Storage</h2>
                            <p>Store your data securely.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <i class="icon-custom icon-md rounded-x icon-bg-dark fa fa-bolt" style="background-color:#558C89;"></i>
                        <div class="content-boxes-in-v3" style="margin-top:1%;">
                            <h2 class="heading-sm" style="color:#00802b">Access Information</h2>
                            <p>Search and access information with ease.</p>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        
        <div class="about-image bg-grey">
            <div class="container">
                <div class="title-v1">
                    <h1>We are CICNP</h1>
                    <p align="justify">Co-operative Information Center Nepal (cicnp) or Sahakari Suchana Kendra (SSK) is the pioneer organization established with the aim of managing the information digitally related to cooperatives and other financial institutions in Nepal. SSK believes in the philosophy of the movement along with the time and what market demands. And in this 21st century, digital transformation has been the need for each and every organization whether it's private organization or its public organization.</p> 
                    <p align="justify">At the same time, in Cooperative Sector there are several complications and challenges, based in the digital transformation and maturity level. Many efforts has been done in order to manage the system for Cooperative, however all those efforts couldn’t provide satisfactory outcome as the underlying resources, data and information flows and integrations were not at the systematic place. To cope with the dynamic environment, now traditional cooperatives will have to adapt the new operating models like changes in IT, new product and service development in digital era, which will be the key factors for being success and moving ahead. SSK helps in risk management by providing the necessary information's to financial institutions. SSK plays crucial role in maintaining transparency in cooperative credit market as well. Lender and Borrower can communicate and exchange information's via this online portal. The trend of using credit from various cooperatives and ultimately ending being non-performing loan has been the major issue for today's cooperative sector. And SSK is here trying to maintain sound credit environment and systematically maintain the cooperative sector digitally. </p>               
                </div>
                <div class="img-center">
                    <img class="img-responsive" src="./assets/img/new/find.jpg" alt="">
                </div>
            </div>
        </div>        

        <div class="container content-lg">
            <div class="title-v1">
                <h2>Our Vision And Mission</h2>
                <p>We are <strong>focused</strong> on maintaining sound cooperative environment<br> 
                </p>
            </div>

            <div class="row">
                <div class="col-md-6 content-boxes-v3 margin-bottom-40">
                    <div class="clearfix margin-bottom-30">
                        <i class="icon-custom icon-md rounded-x icon-bg-u icon-line icon-trophy"></i>
                        <div class="content-boxes-in-v3">
                            <h2 class="heading-sm" style="margin-top:21px">Connecting the dots (Cooperatives)</h2>
                            <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p> -->
                        </div>
                    </div>
                    
                    <div class="clearfix margin-bottom-30">
                        <i class="icon-custom icon-md rounded-x icon-bg-u icon-line icon-printer"></i>
                        <div class="content-boxes-in-v3">
                            <h2 class="heading-sm" style="margin-top:21px">Making paperless reporting system</h2>
                            <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p> -->
                        </div>
                    </div>

                    <div class="clearfix margin-bottom-30">
                        <i class="icon-custom icon-md rounded-x icon-bg-u icon-line icon-directions"></i>
                        <div class="content-boxes-in-v3">
                            <h2 class="heading-sm" style="margin-top:21px">Integrating technology for managing data digitally</h2>
                            <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p> -->
                        </div>
                    </div>

                    <div class="clearfix margin-bottom-30">
                        <i class="icon-custom icon-md rounded-x icon-bg-u icon-line icon-rocket"></i>
                        <div class="content-boxes-in-v3">
                            <h2 class="heading-sm" style="margin-top:12px">Connecting everything as we move to the “Internet of Everything”</h2>
                            <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="./assets/img/new/secure.jpg" alt="">
                </div>
            </div>    
        </div>        

        <div class="parallax-quote parallaxBg">
            <div class="container">
                <div class="parallax-quote-in">
                    <p>If you can design one thing <span style="color:#5E9693;">you can design</span> everything. <br> Just Believe It.</p>
                    <small>- CICNP -</small>
                </div>
            </div>
        </div>        

        <!-- <div class="team-v1 bg-grey content-lg">
            <div class="container">
                <div class="title-v1">
                    <h2>Meet Our Team</h2>
                    <p>We <strong>meet</strong> and get to know you. You tell us and we listen. <br> 
                    </p>
                </div>

                <ul class="list-unstyled row">
                    <li class="col-sm-3 col-xs-6 md-margin-bottom-30">
                        <div class="team-img">
                            <img class="img-responsive" src="./assets/img/new/ganesh.jpg" alt="" style="height:300px;width:300px;">
                            <ul>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <h3>Mr. Ganesh</h3>
                        <h4>/ Managing Director</h4>
                    </li>
                    <li class="col-sm-3 col-xs-6 md-margin-bottom-30">
                        <div class="team-img">
                            <img class="img-responsive" src="./assets/img/new/kumar.jpg" alt="" style="height:300px;">
                            <ul>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <h3>Mr. Kumar</h3>
                        <h4>/ Lead System Analyst</h4>
                    </li>
                    <li class="col-sm-3 col-xs-6">
                        <div class="team-img">
                            <img class="img-responsive" src="./assets/img/new/prasun.jpg" alt="" style="height:300px;">
                            <ul>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <h3>Mr. Prasun</h3>
                        <h4>/ Web Developer</h4>
                    </li>
                    <li class="col-sm-3 col-xs-6">
                        <div class="team-img">
                            <img class="img-responsive" src="./assets/img/new/sabitri.jpg" alt="" style="height:300px;">
                            <ul>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <h3>Mrs. Sabitri</h3>
                        <h4>/ Chief Consultant</h4>
                    </li>
                    <li class="col-sm-3 col-xs-6">
                        <div class="team-img">
                            <img class="img-responsive" src="./assets/img/new/rupa.png" alt="" style="height:300px;">
                            <ul>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <h3>Ms. Rupa</h3>
                        <h4>/ Chief Consultant</h4>
                    </li>
                    <li class="col-sm-3 col-xs-6">
                        <div class="team-img">
                            <img class="img-responsive" src="./assets/img/new/sankar.png" alt="" style="height:300px;">
                            <ul>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-custom icon-sm rounded-x fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <h3>Mr. Sankar</h3>
                        <h4>/ Chief Legal Consultant</h4>
                    </li>
                </ul>                
            </div>
        </div>
 -->
            
    </section>
    <!--  About Section -->

    <!-- Services Section -->
    <section id="services">
        <div class="container content-lg">
            <div class="title-v1">
                <h2>Our Services</h2>
                <p>We do <strong>things</strong> differently company providing key digital services. <br> 
                Focused on helping our clients to build a <strong>successful</strong> business on web and mobile.</p>                
            </div>            
    
            <div class="row service-box-v1">
                <div class="col-md-4 col-sm-6">
                    <div class="service-block service-block-default">
                        <i class="icon-custom icon-lg icon-bg-u rounded-x fa fa-lightbulb-o"></i>
                        <h2 class="heading-md">Access Data and Information</h2>
                        <p></p>
                        <ul class="list-unstyled">
                            
                            <li>Search users</li>
                            <li>Report Generation</li>
                            <li>Access to the borrowers profile</li>
                           
                        </ul>                        
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-block service-block-default">
                        <i class="icon-custom icon-lg icon-bg-u rounded-x fa fa-cloud"></i>
                        <h2 class="heading-sm">Save and Secure your data</h2>
                        <p></p>
                        <ul class="list-unstyled">
                            
                            <li>Easy and Facilitated Service</li>
                            <li>Email notifications</li>
                            <li>Secure and Save your records</li>
                           
                        </ul>                        
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="service-block service-block-default">            
                        <i class="icon-custom icon-lg icon-bg-u rounded-x icon-line icon-users"></i>
                        <h2 class="heading-sm">For Individual Users</h2>
                        <p></p>
                        <ul class="list-unstyled">
                            <li>Apply for Loan</li>
                            <li>Easy and Free Signup</li>
                            <li>Cost Reduction</li>
                            
                            
                        </ul>                        
                    </div>
                </div>
            </div>
        </div>

        
                     
    </section>
    <!-- End Services Section -->

    <!-- News Section -->
    
    <!-- End News Section -->

    <!-- Portfolio Section -->
    
<!--end-->
<section id="testimonials">
        <div class="clients-section parallaxBg">
            <div class="container">
                <div class="title-v1">
                    <h2>Testimonials</h2> <!--our clients-->
                </div>            
                <!-- <ul class="owl-clients-v2">
                    <li class="item"><a href="#"><img src="./assets/img/clients/national-geographic.png" alt=""></a></li>
                    <li class="item"><a href="#"><img src="./assets/img/clients/inspiring.png" alt=""></a></li>
                    <li class="item"><a href="#"><img src="./assets/img/clients/fred-perry.png" alt=""></a></li>
                    <li class="item"><a href="#"><img src="./assets/img/clients/emirates.png" alt=""></a></li>
                    <li class="item"><a href="#"><img src="./assets/img/clients/baderbrau.png" alt=""></a></li>
                    <li class="item"><a href="#"><img src="./assets/img/clients/inspiring.png" alt=""></a></li>
                </ul>          -->   
            </div>
        </div>

        <div class="testimonials-v3">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <ul class="list-unstyled owl-ts-v1">
                            <li class="item">
                                <img class="rounded-x img-bordered" src="./assets/img/new/rajanlamsal.png" alt="">
                                <div class="testimonials-v3-title">
                                    <p style="color:#5E9693;">Mr. Rajan Lamsal</p>
                                    <span>Sahakari Karmi</span>
                                </div>
                                <p>I just wanted to tell you how much I like to use CICNP.<strong> It's very useful to store and retreive information!</strong> <br>
                                Good job, and keep going!<p>
                            </li>
                            <li class="item">
                                <img class="rounded-x img-bordered" src="./assets/img/new/anojpandey.png" alt="">
                                <div class="testimonials-v3-title">
                                    <p style="color:#5E9693;">Mr. Anoj Pandey</p>
                                    <span>Sahakari Karmi</span>
                                </div>                                
                                <p>This platform is very useful for those who wants to maintain sound credit system. Best of Luck guys! </p>
                            </li>
                            <!-- <li class="item">
                                <img class="rounded-x img-bordered" src="./assets/img/team/img1-sm.jpg" alt="">
                                <div class="testimonials-v3-title">
                                    <p style="color:#5E9693;">Nabin Poudel</p>
                                    <span>Marketing &amp; Cunsulting, Himalayan Bank</span>
                                </div>
                                <p>So far I really like the application. I am looking forward to exploring more of your functions. Thank you!</p>
                            </li> -->
                        </ul>
                    </div>                    
                </div>
            </div>
        </div>                 
    </section>
    <!-- End Portfolio Section -->

    <!-- Contact Section -->
    <section id="contact" class="contacts-section">
        <div class="container content-lg">
            <div class="title-v1">
                <h2>Contact Us</h2>
                <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
                It has been the industry's standard dummy text.</p> -->
            </div>

            <div class="row contacts-in">
                <div class="col-md-6 md-margin-bottom-40">
                    <ul class="list-unstyled">
                        <li><i class="fa fa-home"></i> Kathmandu, Nepal</li>
                        <li><i class="fa fa-phone"></i> 9851198580</li>
                        <li><i class="fa fa-envelope"></i> <a href="info@example.com">admin@cicnp.com</a></li>
                        <li><i class="fa fa-globe"></i> <a href="http://htmlstream.com">www.cicnp.com</a></li>
                    </ul>
                </div>

                <div class="col-md-6">
                    <form action="assets/php/sky-forms-pro/demo-contacts-process.php" method="post" id="sky-form3" class="sky-form contact-style">
                        <fieldset>
                            <label>Name</label>
                            <div class="row">
                                <div class="col-md-7 margin-bottom-20 col-md-offset-0">
                                    <div>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                </div>                
                            </div>
                            
                            <label>Email <span class="color-red">*</span></label>
                            <div class="row">
                                <div class="col-md-7 margin-bottom-20 col-md-offset-0">
                                    <div>
                                        <input type="text" name="email" id="email" class="form-control">
                                    </div>
                                </div>                
                            </div>
                            
                            <label>Message</label>
                            <div class="row">
                                <div class="col-md-11 margin-bottom-20 col-md-offset-0">
                                    <div>
                                        <textarea rows="8" name="message" id="message" class="form-control"></textarea>
                                    </div>
                                </div>                
                            </div>
                            
                            <p><button type="submit" class="btn-u btn-brd btn-brd-hover btn-u-dark">Send Message</button></p>
                        </fieldset>

                        <div class="message">
                            <i class="rounded-x fa fa-check"></i>
                            <p>Your message was successfully sent!</p>
                        </div>
                    </form> 
                </div>
            </div>            
        </div>

        <div class="copyright-section">
            <p>2016 &copy; All Rights Reserved. CICNP </p>
            <ul class="social-icons">
                <li><a href="#" data-original-title="Facebook" class="social_facebook rounded-x"></a></li>
                <li><a href="#" data-original-title="Twitter" class="social_twitter rounded-x"></a></li>
                <li><a href="#" data-original-title="Goole Plus" class="social_googleplus rounded-x"></a></li>
                <li><a href="#" data-original-title="Pinterest" class="social_pintrest rounded-x"></a></li>
                <li><a href="#" data-original-title="Linkedin" class="social_linkedin rounded-x"></a></li>
            </ul>
            <span class="page-scroll"><a href="#intro"><i class="fa fa-angle-double-up back-to-top"></i></a></span>
        </div>
    </section>
    <!-- End Contact Section -->

    <!-- JS Global Compulsory -->
    <script type="text/javascript" src="./assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/jquery/jquery-migrate.min.js"></script>    
    <script type="text/javascript" src="./assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="./assets/plugins/smoothScroll.js"></script>    
    <script type="text/javascript" src="./assets/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/pace/pace.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/jquery.parallax.js"></script>
    <script type="text/javascript" src="./assets/plugins/counter/waypoints.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/counter/jquery.counterup.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="./assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>    
    <script type="text/javascript" src="./assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="./assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
    <!-- JS Page Level-->
    <script type="text/javascript" src="./assets/js/one.app.js"></script>
    <script type="text/javascript" src="./assets/js/forms/login.js"></script>
    <script type="text/javascript" src="./assets/js/forms/contact.js"></script>
    <script type="text/javascript" src="./assets/js/plugins/pace-loader.js"></script>
    <script type="text/javascript" src="./assets/js/plugins/owl-carousel.js"></script>
    <script type="text/javascript" src="./assets/js/plugins/revolution-slider.js"></script>
    <script type="text/javascript" src="./assets/js/plugins/cube-portfolio/cube-portfolio-lightbox.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            App.initCounter();
            App.initParallaxBg();
            LoginForm.initLoginForm();
            ContactForm.initContactForm();
            OwlCarousel.initOwlCarousel();
            RevolutionSlider.initRSfullScreen();
        });
    </script>
    <!--[if lt IE 9]>
        <script src="assets/plugins/respond.js"></script>
        <script src="assets/plugins/html5shiv.js"></script>
        <script src="assets/js/plugins/placeholder-IE-fixes.js"></script>
        <script src="assets/plugins/sky-forms-pro/skyforms/js/sky-forms-ie8.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
        <script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.placeholder.min.js"></script>
    <![endif]-->    
</body>
</html>