<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_m extends CI_Model {

	function getcustomer($limit,$offset){
		$this->db->select('customer.*,cooperative_list.name')
		->from('customer')
		  ->join('cooperative_list','customer.org_id=cooperative_list.org_id');
		   $this->db->limit($limit,($offset*$limit));
		        $this->db->order_by('customer.created_date','desc');
	return $this->db->get()->result();

	}
	function insert($data){
               return $this->db->replace('customer',$data);  
         }

         	 function insert_file($input){
  return $this->db->insert('file',$input);
}


	public function getcustomerbyorg($name){

$where='';
$where.="cooperative_list.name LIKE '" .$name."%'";
		$this->db->select('customer.*,cooperative_list.name')->from('customer')
		->join('cooperative_list','cooperative_list.org_id=customer.org_id')
		->where($where)
		->order_by('customer.id','desc');
		return $this->db->get()->result();
	}
		public function getcustomerbyname($name){

$where='';
$where.="customer.customer_name LIKE '" .$name."%'";
		$this->db->select('customer.*,cooperative_list.name')->from('customer')
		->join('cooperative_list','cooperative_list.org_id=customer.org_id')
		->where($where)
		->order_by('customer.id','desc');
		return $this->db->get()->result();
	}



public function searchterm_handler($searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata('search_type', $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata('search_type'))
		{
			$searchterm = $this->session->userdata('search_type');
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}

	public function searchvalue_handler($searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata('search_value', $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata('search_value'))
		{
			$searchterm = $this->session->userdata('search_value');
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}

		function totalcustomer(){
  $this->db->select('id')
  ->from('customer');
  return $this->db->get()->num_rows();
}

function smslog(){
	$this->db->select('sms_log.*,cooperative_list.name,customer.customer_name')->from('sms_log')
	->join('cooperative_list','cooperative_list.org_id=sms_log.org_id')
	->join('customer','customer.id=sms_log.customer_id')

	->order_by('created_date','desc');
	return $this->db->get()->result();
}

}
