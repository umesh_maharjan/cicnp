
<?php foreach($org as $r){
	$name=$r->name;
	$addres=$r->address;
	$contact=$r->contact_no;
	$contact_person=$r->contact_person;

	$email=$r->email;
	$reg_no=$r->reg_no;
	$reg_date=$r->reg_date;
	$purpose=$r->purpose;
	$expired_date=$r->expired_date;

}?>

<?php 
$now = time();
$your_date = strtotime($expired_date);
$datediff =  $your_date-$now;

$expireddate=floor($datediff / (60 * 60 * 24));

	?>
<br>


<style>
  #nav_setting .dropdown-toggle
{
    background-color:#74AFAD;

  color:white !important;
}

	.org_td{
		text-align: center;

	}
</style>


<div class="container">

 <div class="jumbotron" style="background-color:#D9853B; color:white"> 
       
        
        
        <div class ="row">
        
        <img  class="img-circle" style="float:left"; src="<?php echo base_url();?>uploads/organization/<?php echo $this->session->userdata('photo');?>"
 width="100" height="100">
     <?php if($expireddate<='30')
{ ?>  
	        <div class="col-md-10"><h1 text-align:"center">Welcome <?php echo ucwords($this->session->userdata('name')) ?></h1>
</div> <div class="col-md-2">Account will expired after <?php echo $expireddate;?>Days

</div>
	<?php } else {?>
         <div class="col-md-10"><h1 text-align:"center">Welcome <?php echo ucwords($this->session->userdata('name')) ?></h1>
</div>
<?php }?>
      </div>

    </div>


<H3 style="text-align:center"> Organization Detail</H3>



<?php if($this->session->flashdata('success')){
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}
if($this->session->flashdata('error')){
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('error');
	echo '</div>';
} ?>


<table class=" table table-striped table-hover table-responsive">



		<tr>
	
				<td>
					Name:</td>
				<td>
					<?php echo ucfirst($name);?></td>
		
			</tr>

				<tr>
	
				<td>
					Address:
				</td><td>
				
					<?php echo ucfirst($addres);?>
				</td>
			<tr>
			<td>
					Contact Number: 
				</td>
				<td>
									<?php echo $contact;?>
			</td>

			</tr>
			<tr>
			<td>
					Contact Person:
					</td>
				<td>
					<?php echo ucwords($contact_person);?>
					</td>
			</tr>

			<tr><td>
					Email:
				</td>
				<td>
					<?php echo $email;?>
				</td>
				</tr>

			<tr><td>
				
					Registration Number:
				</td>
				<td>
					<?php echo $reg_no;?>
				</td>
				</tr>
			<tr>
			<td>
			
					Registration Date:
				</td><td>
			
					<?php echo $reg_date;?>
				</td>
				</tr>
				<tr>
			<td>
					Purpose:
				</td><td>
					<?php echo $purpose;?>
		</td>
		</tr>

		<tr>
			<td>
					Expired Date:
				</td><td>
					<?php echo $expired_date;?>
		</td>
		</tr>

			</tr>	

	</table>
	<br>
	 <h3 align="center">Board of Director list </h3>

		<table class=" table table-bordered table-striped table-hover table-responsive" style="margin-top:20px;">
		<tr>
			<th>S.N</th>
			<th>Photo</th>

			<th>Name</th> 
			<th>Address</th>
			<th>Gender</th>
			<th>Contact Number</th>
			
			<th>Education Qualification</th>	
			
		</tr>

		
		<?php 

		if(!empty($bod)){
			$sn = 1;
			foreach($bod as $r){ 

				$contactarray = explode(',', $r->contact);
				$eduarray = explode(',', $r->edu_qulfy);?>
			<tr> 

				<th> <?php echo $sn?>
				</th>
				<th> <img src="<?php echo base_url();?>uploads/bodphoto/<?php echo $r->photo;?>" width="80" height="80">
				</th>

				<th> <?php echo ucwords($r->name);?>
				</th>
				<th> <?php echo ucwords($r->address);?>
				</th>
				
				<th> <?php echo $r->gender;?>
				</th>
				<th> 
						<?php foreach($contactarray as $c){
							?>
							<li>
								<?php echo $c;?></li>
								<?php 
							}?>

						</th>
					
						<th> 
							<?php foreach($eduarray as $edu){
								?>
								<li>
									<?php echo $edu;?></li>
									<?php 
								}?>

							</th>

				
			</tr>
			
			<?php  $sn++;} } else {
				?>
					
				</table>
		
			<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
There is no  Board of   Director list</div>
<?php } ?>



<table class=" table table-bordered table-hover table-responsive" style="margin-top:20px;">
<br>
<br>
 <h3 align="center">Loan Committee Management list </h3>
		<tr>
			<th>S.N</th>
			<th>Photo</th>

			<th>Name</th>
			<th>Address</th>
			<th>Contact Number</th>
			<th>Gender</th>
			<th>Education qualification</th>

		
			
			
			
		</tr>

		
		<?php 

		if(!empty($lcm)){
			$sn = 1;
			foreach($lcm as $r){ 
				$contactarray = explode(',', $r->contact);
				$eduarray = explode(',', $r->edu_qulfy);?>



				<tr> 

					<th> <?php echo $sn?>
					</th>
					<th> <img src="<?php echo base_url();?>uploads/lcm/<?php echo $r->photo;?>" width="80" height="80">
					</th>

					<th> <?php echo ucwords($r->name);?>
					</th>
					<th> <?php echo $r->address;?>
					</th>
					<th> 
						<?php foreach($contactarray as $c){
							?>
							<li>
								<?php echo $c;?></li>
								<?php 
							}?>

						</th>
						<th> <?php echo $r->gender;?>
						</th>
						<th> 
							<?php foreach($eduarray as $edu){
								?>
								<li>
									<?php echo $edu;?></li>
									<?php 
								}?>

							</th>




						
							
						</tr>
						<?php  $sn++;} 
					} else {
						?>
						

											
						
					</table>
<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
There is no  Loan Committee list</div>
<?php } ?>






	<table class=" table table-bordered table-hover table-responsive" style="margin-top:20px;">

	<br>
<br>
 <h3 align="center">Operation Management Team List </h3>
		<tr>
			<th>S.N</th>
			<th>Name</th>
			<th>Position</th>
			
		</tr>

		
		<?php 

		if(!empty($omt)){
			$sn = 1;
			foreach($omt as $r){ 
				?>
				<tr> 

					<th> <?php echo $sn?>
					</th>

					<th> <?php echo ucwords($r->name);?>
					</th>
					<th> <?php echo ucfirst($r->position);?>
					</th>

					
					
				</tr>
				<?php  $sn++;} } else {
					?>
					

				
					
				
				</table>
<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
There is no Operation Management Team list</div>
<?php } ?>


				</div>



