<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrganizationCustomer extends CI_Controller {

  function __construct()
  {
    
   parent:: __construct();
   $this->load->helper(array('form','url','html','date'));
$this->load->model('Organizationcustomer_m');
$this->load->model('Customer_m');
  	$this->load->library('pagination');
 }

 function index(){
 	$this->load->view('header');
 	$this->load->view('menu');

  $id=$this->session->userdata('id');


$total_row = $this->Organizationcustomer_m->totalcustomer($id);
  $config = array();
$config["base_url"] = base_url() . "OrganizationCustomer/index";
  $config['use_page_numbers']  = TRUE;
  $config["per_page"] = 2;
  $config['total_rows']= $total_row;
  $config['full_tag_open'] = '<ul class="pagination">';
  $config['full_tag_close'] = '</ul>';
  $config['first_link'] = false;
  $config['last_link'] = false;
  $config['first_tag_open'] = '<li>';
  $config['first_tag_close'] = '</li>';
  $config['prev_link'] = '&laquo';
  $config['prev_tag_open'] = '<li class="prev">';
  $config['prev_tag_close'] = '</li>';
  $config['next_link'] = '&raquo';
  $config['next_tag_open'] = '<li>';
  $config['next_tag_close'] = '</li>';
  $config['last_tag_open'] = '<li>';
  $config['last_tag_close'] = '</li>';
  $config['cur_tag_open'] = '<li class="active"><a href="#">';
  $config['cur_tag_close'] = '</a></li>';
  $config['num_tag_open'] = '<li>';
  $config['num_tag_close'] = '</li>';
  $this->pagination->initialize($config);
  if($this->uri->segment(3)){
    $page = ($this->uri->segment(3)-1) ;
  }
  else{
    $page = 0;
  }

  $data['sn']=($config["per_page"]*$page)+1;
  $data["links"] = $this->pagination->create_links();

	$data['customer']=$this->Organizationcustomer_m->getcustomer($config["per_page"],$page,$id);
	

 	$this->load->view('org_customer/customerlist',$data);
 }

  function searchlist(){
 	$id=$this->session->userdata('id');

$searchvalue = $this->Customer_m->searchvalue_handler($this->input->get_post('search_item', TRUE));
if($this->input->get_post('search') && $this->input->get_post('search_item')){
  redirect('OrgainzationCustomer/searchlist');
}


    $data['customer']=$this->Organizationcustomer_m->getcustomerbyname($searchvalue,$id);
   $this->load->view('header');
   $this->load->view('menu');
 	$this->load->view('org_customer/search_customerlist',$data);

 

 }

 function add(){
    $this->load->view('header');
  $this->load->view('menu');

  $this->load->view('org_customer/add_org_customer');
 }

 function add_customer(){
    $id=$this->session->userdata('id');
    $params=array();
$params['org_id']= $id;
    $params['created_date']=date('Y-m-d');
    $params['customer_name']=$this->input->post('customer_name');
     $customer_id=$this->input->post('customer_id');
     $params['customer_id']=$id.'/'.$customer_id;
     $params['address']=$this->input->post('address');
      $params['contact']=$this->input->post('contact');
      $data=$this->Organizationcustomer_m->checkcustomer_id($params['customer_id']);
      if($data){
 $this->session->set_flashdata('error', 'Customer code already used');  
  redirect('OrganizationCustomer');  
      }else{
        $customer=$this->Organizationcustomer_m->insert($params);
        if($customer){
         
$this->session->set_flashdata('success', 'Customer  added successfully');
 redirect('OrganizationCustomer');  
        }else{
     
$this->session->set_flashdata('error', 'Error to add customer please add again');  
  redirect('OrganizationCustomer');
        }
      }


 }
 function editview(){
      $this->load->view('header');
      $id=$this->input->get('id');
      $data['customer']=$this->Organizationcustomer_m->getcustomerbyid($id);

  $this->load->view('menu');
  $this->load->view('org_customer/edit_org_customer',$data);
 }
function editcustomer(){
 $id=$this->session->userdata('id');
    $params=array();

    $params['created_date']=date('Y-m-d');
    $params['customer_name']=$this->input->post('customer_name');
     $customer_id=$this->input->post('customer_id');
     $params['customer_id']=$id.'/'.$customer_id;
     $params['address']=$this->input->post('address');
      $params['contact']=$this->input->post('contact');
      $primaryid=$this->input->post('id');
      $data=$this->Organizationcustomer_m->checkcustomeid($primaryid,$params['customer_id']); 
     if($data){

      $this->session->set_flashdata('error', 'Customer code already used');  
  redirect('OrganizationCustomer/editview?id='.  $primaryid);
     }
     else{
      $update=$this->Organizationcustomer_m->update($primaryid,$params);
      if($update){
        $this->session->set_flashdata('success', 'Customer edit successfully');  
          redirect('OrganizationCustomer');
      }
     }
}


function delete(){
       if(!$this->session->userdata('login')){
    redirect('Login');

  }
  $this->load->view('header');
  $data['id']=$this->input->get('id');
  $data['customer']=$this->Organizationcustomer_m->getcustomerbyid($data['id']);

  $this->load->view('org_customer/delete_customer',$data);
}


function confirm_delete(){
  $id=$_POST['id'];
  

  $data=$this->Organizationcustomer_m->deletecustomer($id);
  $this->session->set_flashdata('success', 'Delete successfully');
  redirect('OrganizationCustomer'); 
    //}
  //    // else{
  // $this->session->set_flashdata('error', ' Server error  Please try again !');
  // redirect('OrganizationCustomer'); 
  

}

function sendsms(){
  $params=array();
$smsarray=array();
  $org_id=$this->session->userdata('id');
  $totalbalance=$this->Organizationcustomer_m->totalbalance($org_id);
  foreach($totalbalance as $b){
    $tot_balance=$b->total_balance;
    $persms=$b->persms;
  } 

  $params['total_balance']=$tot_balance-('1'*$persms);
  if( $tot_balance<('1'*$persms)){
      $this->session->set_flashdata('error', "Don't have sufficient balance to sent sms please recharge ur account ");
  redirect('OrganizationCustomer');
  }
  $id=$this->input->get('id'); 
    $customer=$this->Organizationcustomer_m->getcustomerbyid($id);

  foreach($customer as $c){
    $contact=$c->contact;
      
  }
     $updatebalance=$this->Organizationcustomer_m->updatebalance($org_id,$params);
    if($updatebalance){

      //sms log
      $smsarray['org_id']=$org_id;
      $smsarray['created_date']=date('Y-m-d');
       $smsarray['customer_id']=$id;

      $smslog=$this->Organizationcustomer_m->smslog($smsarray);
      if($smslog)
{
      $this->session->set_flashdata('success', 'Send sms successfully');
  redirect('OrganizationCustomer');   
    }
  }


}


 function sentsmetoall(){

  $params=array();

  $org_id=$this->session->userdata('id');
  $totalbalance=$this->Organizationcustomer_m->totalbalance($org_id);
  foreach($totalbalance as $b){
    $tot_balance=$b->total_balance;
    $persms=$b->persms;
  } 
  $data =$this->input->post('action_to');
  $totalsms=count($data);
  $params['total_balance']=$tot_balance-($totalsms*$persms);
  if( $tot_balance<($totalsms*$persms)){
      $this->session->set_flashdata('error', "Don't have sufficient balance to sent sms please recharge ur account ");
  redirect('OrganizationCustomer');
  }


    foreach( $data as $d){
      $id=$d;

  $customer=$this->Organizationcustomer_m->getcustomerbyid($id);

  foreach($customer as $c){
    $contact=$c->contact;
      

  }

    //   $api_url = "http://api.sparrowsms.com/v2/sms/?".
    //     http_build_query(array(
    //         'token' => 'Fenlprslyx0h62REadIp',
    //        'from'  => 'Demo',
    //         'to'    => $contact,
    //         'text'  => $message));

    // $response = file_get_contents($api_url);
 $smsarray['org_id']=$org_id;
      $smsarray['created_date']=date('Y-m-d');
       $smsarray['customer_id']=$id;

   $smslog=$this->Organizationcustomer_m->smslog($smsarray);
    }

    $updatebalance=$this->Organizationcustomer_m->updatebalance($org_id,$params);
    if($updatebalance){
      $this->session->set_flashdata('success', 'Send sms successfully');
  redirect('OrganizationCustomer');   
    }


 }


}