<br>
<div class="container">
<h3 style="text-align:center">Add Customer Form</h3>
    <form id="add_user" method="post" action="<?php echo base_url('OrganizationCustomer/add_customer'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>

          <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Customer Code</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="customer_id" id="name" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="customer_name" id="name" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="address" id="postion" style="text-transform: capitalize;" required/>
            </div>  
        </div>
               <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Contact</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" name="contact" id="contact"  required/>
            </div>  
        </div>

        


        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
    </form>
</div>


