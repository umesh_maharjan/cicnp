<?php 
class Black_m extends CI_Model{
	


	public function checkcitizen($citizen,$id){
		$this->db->select('citizen_number')->from('user_detail')
		->where('citizen_number',$citizen)
		->where('organization_id',$id);
		return $this->db->get()->result();
	}

	function insert($input,$tbl){
		return $this->db->insert($tbl, $input);
	}

	public function update($id,$input,$tbl)

	{

		return $this->db->where('black_id', $id)->update($tbl, $input);
	}


	public function getusers($id){
		$this->db->select('user_detail.*,cooperative_list.name as org_name')->from('user_detail')
		->join('cooperative_list','cooperative_list.org_id=user_detail.organization_id')
		->where('user_detail.organization_id',$id)
		->order_by('user_detail.black_id','desc');
		return $this->db->get()->result();
	}

	public function getusersname($id,$name){
$where='';
$where.="user_detail.name LIKE '" .$name."%'";



		$this->db->select('user_detail.*,cooperative_list.name as org_name')->from('user_detail')
		->join('cooperative_list','cooperative_list.org_id=user_detail.organization_id')
		->where('user_detail.organization_id',$id)
		->where($where)
		->order_by('user_detail.black_id','desc');
		return $this->db->get()->result();
	}
	public function getusercitizen($id,$name){
		$this->db->select('user_detail.*,cooperative_list.name as org_name')->from('user_detail')
		->join('cooperative_list','cooperative_list.org_id=user_detail.organization_id')
		->where('user_detail.organization_id',$id)
		->where('user_detail.citizen_number',$name)
		->order_by('user_detail.black_id','desc');
		return $this->db->get()->result();
	}

	public function getblacklistname($name){

$where='';
$where.="user_detail.name LIKE '" .$name."%'";
		$this->db->select('user_detail.*,cooperative_list.name as org_name,cooperative_list.org_id')->from('user_detail')
		->join('cooperative_list','cooperative_list.org_id=user_detail.organization_id')
		->where($where)
		->order_by('user_detail.black_id','desc');
		return $this->db->get()->result();
	}

	public function getallblacklistname($name){

$where='';
$where.="name LIKE '" .$name."%'";
		$this->db->select('*')->from('user_detail')
		
		->where($where)
		->order_by('name','asc');
		return $this->db->get()->result();
	}


	public function getblacklistcitizen($name){
		$this->db->select('user_detail.*,cooperative_list.name as org_name,cooperative_list.org_id')->from('user_detail')
		->join('cooperative_list','cooperative_list.org_id=user_detail.organization_id')
		->where('user_detail.citizen_number',$name)
		->order_by('user_detail.black_id','desc');
		return $this->db->get()->result();
	}

	function getuserdetail($id){

		$this->db->select('user_detail.*,cooperative_list.name as org_name')->from('user_detail')
		->join('cooperative_list','cooperative_list.org_id=user_detail.organization_id')
		->where('user_detail.black_id',$id);
		
		return $this->db->get()->result();

	}
	function getimage($id){
		$this->db->select('bod_photo')->from('cooperative_list')->where('org_id',$id);
		return $this->db->get()->result();
	}

	function chkcitizen($id,$ctzn){
		$this->db->select('citizen_number')->from('user_detail')->where('citizen_number',$ctzn)
		->where('black_id !=',$id);
		return $this->db->get()->result();
	}

	function getphoto($id){
		$this->db->select('photo')->from('user_detail')->where('black_id',$id);
		return $this->db->get()->result();
	}

	 function getblack_id($id){
	 		$this->db->select('*')->from('user_detail')->where('black_id',$id);
		return $this->db->get()->result();
	 }

	 function deleteuser($id){

	return
		$this->db->where('black_id',$id)
		->delete('user_detail');
	 }

	 function getemail($id){
	 	$this->db->select('email,contact_no,name,address')->from('cooperative_list')
	 	->where('org_id',$id);
return $this->db->get()->result();

	 }

	 	 function getorginfo($id){
	 	$this->db->select('*')->from('cooperative_list')
	 	->where('org_id',$id);
return $this->db->get()->result();

	 }


	 	 function deleteblacklist(){

	return
		$this->db->where('organization_id','0')
		->delete('user_detail');
	 }
	}

?>