

<?php foreach($omt as $r){
    $name=$r->name;
    $position=$r->position;
    $id=$r->id;

}?>
<br>
<div class="container">

<h3 style="text-align:center">Edit OMT Form</h3>

    <form id="add_user" method="post" action="<?php echo base_url('management/editomt'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="name" id="name"  value="<?php echo $name;?>"style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Position</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="position" id="position" value="<?php echo $position;?>"style="text-transform: capitalize;" required/>
            </div>  
        </div>

        

        <input type="hidden" name="id" value="<?php echo $id;?>">
        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
    </form>
</div>


