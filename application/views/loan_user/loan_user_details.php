<br>

<div class="container">
<h3 style="text-align:center">Loan Apply Form</h3>
    <form id="loan_form" method="post" action="<?php echo base_url('loan_user/addapplyloan');?>" class="form-horizontal" >

        <div class="form-group">
            <label for="fathername" class="col-sm-2 control-label">Father Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="fathername" id="fathername" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="grandfathername" class="col-sm-2 control-label">Grandfather Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="grandfathername" id="grandfathername" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="purpose" class="col-sm-2 control-label">Purpose</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="purpose" id="purpose" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="monthlyexpense" class="col-sm-2 control-label">Monthly Expense</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" name="monthlyexpense" id="monthlyexpense" style="text-transform: capitalize;" required/>
            </div>  
        </div>


        <div class="form-group">
            <label for="monthlyincome" class="col-sm-2 control-label">Monthly Income</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" name="monthlyincome" id="monthlyincome"  required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="appliedloan" class="col-sm-2 control-label">Applied Loan</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" name="appliedloan" id="appliedloan" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label for="collateral" class="col-sm-2 control-label">Collateral</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="collateral[]" id="collateral" style="text-transform: capitalize;" required/>
            </div>  
            <div class="col-sm-0">
               <button class="btn btn-default" id="add_more_collateral" onclick="addCollateral()">Add More</button>
           </div>
       </div>

       <div id="add_collateral"></div>

       <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-success" style="margin-left:43%;" />

   </form>
</div>

<script type="text/javascript">
	var count= 0;
	var addCollateral = function(){
		count = count+1;
		//console.log(count);
		$("#add_collateral").append("<div class='container' id='collateral"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' class='form-control' name='collateral[]'  style='text-transform: capitalize;' placeholder='Collateral' required/></div><div class='col-sm-0'><button class='btn btn-default' id='collateral"+count+"' onclick='removeCollateral("+count+")'>Remove</button></div></div>");
	};

	var removeCollateral = function(c){
		// console.log("remove count: "+c);
		// console.log("contact"+c);
		$("#collateral"+c).remove();
		
	};

</script>