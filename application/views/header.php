<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <title>CICNP</title>
  <link rel="icon" href="<?php echo site_url('assets/img/new/reckon.png'); ?>" type="image/x-icon" />
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assest/css/bootstrap.min.css'); ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assest/css/font-awesome.css'); ?>" />
  <!--   <link rel="stylesheet" type="text/css" href="<?php echo site_url('assest/css/main.css'); ?>" /> -->

  <script src="<?php echo site_url('assest/js/jquery.js'); ?>"></script>
  <script src="<?php echo site_url('assest/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo site_url('assest/js/smoothScroll.js'); ?>"></script>
  <script src="<?php echo site_url('assest/js/back-to-top.js'); ?>"></script>
  <script src="<?php echo site_url('assest/ckeditor/ckeditor.js'); ?>"></script>
  <script src="<?php echo site_url('assest/js/jquery.timepicker.js'); ?>"></script>

  <style type="text/css">
   @font-face
   {
    font-family: 'Proxima'; /*a name to be used later*/
    src: url('<?php echo base_url();?>assest/fonts/Mark Simonson - Proxima Nova Alt Regular.otf');
  }


  @font-face
  {
    font-family: 'proximanova';
    src: url('<?php echo base_url();?>assest/fonts/ProximaNova-Light.eot') format('eot'), url('<?php echo base_url();?>assest/fonts/ProximaNova-Light.woff') format('woff'), url('fonts/ProximaNova-Light.ttf') format('truetype'), url('fonts/ProximaNova-Light.otf') format('opentype');
  }
  body{
    color:#222;
    background:white;
      font-family: 'Proxima';
  }


</style>
</head>
<body>