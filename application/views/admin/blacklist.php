<br>
<H3 style="text-align:center"> Blacklist List </H3>
<div class="container">

	<?php 
	if($this->session->flashdata('success')){
		echo '<div class="alert alert-success">';
		echo $this->session->flashdata('success');
		echo '</div>';
	}

	?>
	<?php 
	if($this->session->flashdata('error')){
		echo '<div class="alert alert-danger">';
		echo $this->session->flashdata('error');
		echo '</div>';
	}

	?>
	<div class="row" style="align:center;">
		
		<div class="col-md-12"><form action="<?php echo base_url('Login/searchlist');?>" method="post">
			<table>
				<td class="col-md-4" >
					<td class="col-md-3" ><select class="form-control"  name="search">

						<option value="0">--Select type--</option>
						<option value="1">Name</option>
						<option value="2">Citizen number</option>
					</select>
				</td>

				<td class="col-md-3" > <input type="text" name="search_item"  class="form-control" placeholder="Search" required></td>
				<td> <input type="submit"   value="Search" class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;"></td>
			</table>
		</form>

	</div></div>

	<div class="row">
		
	</div>
	<table class=" table table-bordered table-hover table-responsive" style="margin-top:20px;">
		<tr>
			<th>S.N</th>
			<th>Photo</th>

			<th>Name</th>
			<th>Address /Associated person</th>
			<th>Contact Number</th>
		

			<th>Action</th>
		</tr>
		<?php 

		if(!empty($users)){
			$sn = 1;
			foreach($users as $r){ 


				$contactarray = explode(',', $r->contact_no);?>
			<tr> 

				<th> <?php echo $sn?>
				</th>
				<?php if($r->organization_id==0) { ?>

				<th> <img src="<?php echo base_url();?>uploads/black/2960579f1a04ac292.png" width="80" height="80">
</th>
<th> <?php echo $r->name;?>
</th>
<th style="width:30%"> <?php echo $r->permanent_address;?>
</th>
<th>N/A
</th>
<th>N/A
</th>


				<?php } else { ?>
				<th> <img src="<?php echo base_url();?>uploads/black/<?php echo $r->photo;?>" width="80" height="80">
				</th>

				<th> <?php echo ucwords($r->name);?>
				</th>
				<th> <?php echo $r->permanent_address;?>
				</th>
				<th> 
					<?php foreach($contactarray as $c){
							?>
							<li><?php echo $c;?></li>
								<?php 
							}?>

						</th>


			

				<th style="padding:6px"><a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php  echo base_url();?>login/userdetail?id=<?php echo $r->black_id;?>">Detail</i></i></a> 
				
				</th>
				<?php }?>

			</tr>
			<?php  $sn++;} } else {
				?>
			</table>
			<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
				There is no  black list</div>'

				<?php } ?>

			</div>
			<script type="text/javascript">
				document.getElementById('nav_user').className += "active";
			</script>
