<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		
		parent:: __construct();
		$this->load->helper(array('form','url','html','date'));
		$this->load->model('Common_m');

		$this->load->model('Black_m');
		$this->load->model('Management_m');
		$this->load->library('pagination');
		//$this->load->library('usercheck');
	}
	public function index()
	{
		if(!$this->session->userdata('login'))
	{

	$this->load->view('header');
	$this->load->view('admin/login_v');	
	}
	else{
		$usertype=$this->session->userdata('usertype');
			if($usertype=="Admin"){
				redirect('Login/viewprofile');
			}

			else if($usertype=="Loan"){
				redirect('Loan_user/viewprofile');
			}
			else{
				redirect('login/loginsucess');
			}
	
}
}


public function usercheck(){
	$username=$this->input->post('username');
	$password=$this->input->post('password');

	$getsalt=$this->Common_m->getusersalt($username);


	if($getsalt){

		foreach($getsalt as $r){
			$salt=$r->salt;
		}
		$psw=sha1($password.$salt.'Db_Project');
		$users=$this->Common_m->getusersinfo($username,$psw);



		if($users){


			foreach($users as $u){
				$id=$u->org_id;
				$email=$u->email;
				$name=$u->name;
				$usertype=$u->usertype;
				$active=$u->active;
				$photo=$u->photo;
			}

			if($active=='0'){
				$this->session->set_flashdata('error','Your account is not active yet');
				redirect('Login');		
			}  


			$session_array = array('login'=>'true',
				'id'=>$id,
				'email'=>$email,
				'name'=>$name,
				'photo'=>$photo,
				'usertype'=>$usertype);
			$this->session->set_userdata($session_array);

if($usertype=="Admin"){
				redirect('Login/viewprofile');
			}

			else if($usertype=="Loan"){
				redirect('Loan_user/viewprofile');
			}
			else{
				
				redirect('login/loginsucess');
			}
	


		}else{
			$this->session->set_flashdata('error','Email or Password not correct');
			redirect('Login');	
		}



	}
	else{
		$this->session->set_flashdata('error','Email or Password not correct');
		redirect('Login');
	}
}


public function userlogin(){
	if(!$this->session->userdata('login')){
		redirect('Login');
	}

	$id=$this->session->userdata('id');
	$totalloanapplied=$this->Management_m->getloancount();
	$totalviewed=$this->Management_m->getnotificationcount($id);

	$data['count']=$totalloanapplied-$totalviewed;

	$params['usertype']="Admin";
	$this->load->view('header');
	$this->load->view('menu',$data);
	$this->load->view('dashboard',$params);


}


public function loginsucess(){
	if(!$this->session->userdata('login')){
		redirect('Login');
	}

	$params['usertype']="SuperAdmin";
	$this->load->view('header');
	$this->load->view('navigation');
	$this->load->view('dashboard',$params);


}
//loan menu
public function login_success(){
	if(!$this->session->userdata('login')){
		redirect('Login');
	}

	$params['usertype']="Loan";
	$this->load->view('header');
	$this->load->view('loan_menu');
	$this->load->view('dashboard',$params);


}



public function useradd_view(){
	 	// if(!$this->session->userdata('login')){
	 	// 	redirect('login');
	 	// }
	$this->load->view('header');
	$this->load->view('admin/add_user');


}

//get all organization

public function getalluser(){
	if(!$this->session->userdata('login')){
		redirect('Login');

	}
	$usertype=$this->session->userdata('usertype');
	if($usertype!='SuperAdmin'){
		redirect('Login');
	}


	$total_row = $this->Common_m->totalcountorg();
	$config = array();
	$this->load->view('header');
	$this->load->view('navigation');

	$config["base_url"] = base_url() . "login/getalluser";
	$config['use_page_numbers']  = TRUE;
	$config["per_page"] = 100;
	$config['total_rows']= $total_row;
	$config['full_tag_open'] = '<ul class="pagination">';
	$config['full_tag_close'] = '</ul>';
	$config['first_link'] = false;
	$config['last_link'] = false;
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['prev_link'] = '&laquo';
	$config['prev_tag_open'] = '<li class="prev">';
	$config['prev_tag_close'] = '</li>';
	$config['next_link'] = '&raquo';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$this->pagination->initialize($config);
	if($this->uri->segment(3)){
		$page = ($this->uri->segment(3)-1) ;
	}
	else{
		$page = 0;
	}

	$data['sn']=($config["per_page"]*$page)+1;
	$data["links"] = $this->pagination->create_links();





	$data['users']=$this->Common_m->getusers($config["per_page"],$page);
	$this->load->view('admin/user',$data);
}



public function getusersloans(){
	if(!$this->session->userdata('login')){
		redirect('Login');

	}
	$usertype=$this->session->userdata('usertype');
	if($usertype!='SuperAdmin'){
		redirect('Login');
	}
	$this->load->view('header');
	$this->load->view('navigation');


	$total_row = $this->Common_m->totalcountloan();

	$config["base_url"] = base_url() . "login/getusersloans";
	$config['use_page_numbers']  = TRUE;
	$config["per_page"] = 100;
	$config['total_rows']= $total_row;
	$config['full_tag_open'] = '<ul class="pagination">';
	$config['full_tag_close'] = '</ul>';
	$config['first_link'] = false;
	$config['last_link'] = false;
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['prev_link'] = '&laquo';
	$config['prev_tag_open'] = '<li class="prev">';
	$config['prev_tag_close'] = '</li>';
	$config['next_link'] = '&raquo';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$this->pagination->initialize($config);
	if($this->uri->segment(3)){
		$page = ($this->uri->segment(3)-1) ;
	}
	else{
		$page = 0;
	}
	$data['sn']=($config["per_page"]*$page)+1;

	$data["links"] = $this->pagination->create_links();

	$data['users']=$this->Common_m->getusersloans($config["per_page"],$page);
	$this->load->view('loan_user/loan_user',$data);
}



public function adduser() {




	$params=array();

	$params['name']=$this->input->post('name');
	$params['address']=$this->input->post('address');
	$params['email']=$this->input->post('email');
	$params['reg_no']=$this->input->post('rege_no');   

//$params['contact_no']=$this->input->post('contact');
	$params['contact_person']=$this->input->post('cont_person');
	$params['reg_date']=$this->input->post('rege_date');
	$params['purpose']=$this->input->post('purpose');
	$password= $this->input->post('password');
	$c_password = $this->input->post('c_password');
	$params['active']='0';

	$params['usertype']='Admin';


	$contact=$this->input->post('contact');
	for($i=0;$i<count($contact);$i++){
		$data_c[]= $contact[$i];
	}
	$params['contact_no']=implode(",",$data_c);

	$b=rand();
	$key =sha1($b);
	$params['salt']=substr(md5(uniqid(rand(), true)), 0, 6);
	$params['Password']=sha1($password.$params['salt'].'Db_Project');

	$checkreg=$this->Common_m->checckreg($params['reg_no']);
	$checkmail=$this->Common_m->checckemail($params['email']);

	$config['upload_path'] = './uploads/organization';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['overwrite']    =  TRUE;

				//$config['encrypt_name'] = true;

	$config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
				// $config['max_height']  = '720';   
	$config['overwrite']    =  TRUE;
	$config['file_name']  = uniqid(rand());	
	$this->load->library('upload', $config);
	$this->upload->initialize($config);

	$type= explode('.',$_FILES['image']['name']);
	$type= $type[count($type)-1];

	$params['photo'] = $config['file_name'].'.'.$type;


	$upload = $this->upload->data();


	if(!$this->upload->do_upload('image'))

	{


		$error = array('error' => $this->upload->display_errors());              
		$this->session->set_flashdata('error', $this->upload->display_errors());  
		redirect('Login/useradd_view');
	}


	else if($checkmail){
		$this->session->set_flashdata('error','Email already exist');
		redirect('Login/useradd_view');
	}
	else if ($checkreg){
		$this->session->set_flashdata('error','Registration number already used');
		redirect('Login/useradd_view');
	}
	else if($password!=$c_password){
		$this->session->set_flashdata('error','Password mismatch');

		redirect('Login/useradd_view');

	}

	else{
		$data=$this->Common_m->insert($params,'cooperative_list');


		$this->session->set_flashdata('success', 'Please contact with administrator to active  your account ');
		redirect('Login');


	}




}
//organization detail information
public function organizationdetail(){
	if(!$this->session->userdata('login')){
		redirect('Login');

	}
	$usertype=$this->session->userdata('usertype');
	if($usertype!='SuperAdmin'){
		redirect('Login');
	}
	$id=$this->input->get('org');

	$data['bod']=$this->Common_m->getbod($id);
	$data['lcm']=$this->Common_m->getlcm($id);
	$data['omt']=$this->Common_m->getomt($id);
	$data['org']=$this->Common_m->getorg($id);
	$this->load->view('header');
	$this->load->view('navigation');
	$this->load->view('admin/org_detail',$data);
}


function getblacklist(){

	if(!$this->session->userdata('login')){
		redirect('login');
	}
	$id=$this->input->get('org');
	$data['users']=$this->Common_m->getblacklist($id);


	$this->load->view('header');
	$this->load->view('navigation');
	$this->load->view('admin/blacklist',$data);
}


function userdetail(){
	if(!$this->session->userdata('login')){
		redirect('Login');
	}


	$org_id=$this->session->userdata('id');


	$id=$this->input->get('id');
	$data['black']=$this->Black_m->getuserdetail($id);
	$this->load->view('header');
	$this->load->view('navigation');
	$this->load->view('user/userdetail',$data);
}




//loan detail information

public function loandetail(){

	if(!$this->session->userdata('login')){
		redirect('login');
	}
	$id=$this->input->get('org');
	$data['org']=$this->Common_m->getloan_info($id);


	$this->load->view('header');
	$this->load->view('navigation');
	$this->load->view('loan_user/loan_detail',$data);
}


public function changemail(){
	if(!$this->session->userdata('login')){
		redirect('Login');

	}
	$usertype=$this->session->userdata('usertype');
	if($usertype!='SuperAdmin'){
		redirect('Login');
	}

	$this->load->view('header');
	$this->load->view('navigation');

	$this->load->view('admin/changeemail');

}
function updateemail(){
	$id=$this->session->userdata('id');
	$data['email']=$this->input->post('email');
	$passwords=$this->input->post('password');
	$params=$this->Common_m->getsalt($id);
	foreach($params as $r){
		$email=$r->email;
		$salt=$r->salt;
		$password=$r->password;
	}


	$psw=sha1($passwords.$salt.'Db_Project');



	$checkemail=$this->Common_m->checkdublicatedemail($data['email']);


	if($email==$data['email']){
		$this->session->set_flashdata('error',' Old email address'); 
		redirect('login/changemail');	   	}

		else if($password != $psw){
			$this->session->set_flashdata('error','Incorrect Password');
			redirect('login/changemail');	
		}
		else if($checkemail){
			$this->session->set_flashdata('error','Email already exists'); 
			rredirect('login/changemail');	

		}
		else{
			$update=$this->Common_m->update($id,$data,'cooperative_list');
			if($update){
				$this->session->set_flashdata('success','Email successfully changed'); 
				redirect('login/changemail');	

			}
			else{
				$this->session->set_flashdata('error','Server error please try it again !'); 
				redirect('login/changemail');	
			}
		}
	}


	public function changepassword(){
		if(!$this->session->userdata('login')){
			redirect('Login');

		}
		$usertype=$this->session->userdata('usertype');
		if($usertype!='SuperAdmin'){
			redirect('Login');
		}

		$this->load->view('header');
		$this->load->view('navigation');

		$this->load->view('admin/changepassword');
	}

	public function changepasswordorg(){
		$usertype=$this->session->userdata('usertype');
		if($usertype!='Admin'){
			redirect('Login');
		}

		$id=$this->session->userdata('id');

		$totalloanapplied=$this->Management_m->getloancount();
		$totalviewed=$this->Management_m->getnotificationcount($id);

		$data['count']=$totalloanapplied-$totalviewed;

		$this->load->view('header');
		$this->load->view('menu',$data);

		$this->load->view('header');
		$this->load->view('menu',$data);
		$this->load->view('admin/changepassword');
	}

	public function updatepassword(){
		$id=$this->session->userdata('id');
		$oldpassword = $this->input->post('old_password');
		$newpassword=$this->input->post('new_password');
		$confirm_pw = $this->input->post('confirm_password');

		$checkoldpassword=$this->Common_m->getorg($id);

		foreach($checkoldpassword as $p){
			$old_pass=$p->password;
			$salt=$p->salt;

		}

		$psw=sha1($oldpassword.$salt.'Db_Project');
		if($psw !=$old_pass){
			$this->session->set_flashdata('error', 'Old password not match');
			redirect('login/changepassword');
		}else if($oldpassword==$newpassword){
			$this->session->set_flashdata('error', 'New password same as old password');
			redirect('login/changepassword');
		}
		else if($newpassword != $confirm_pw){
			$this->session->set_flashdata('error', 'New password and confirm password not match ');
			redirect('login/changepassword');
		}
		else{
			$data['password']=sha1($newpassword.$salt.'Db_Project');
			$this->Common_m->update($id,$data,'cooperative_list');
			$this->session->set_flashdata('success', 'Password change successfully');
			redirect('login/changepassword');
		}
	}

	function logout(){
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('email');
		$this->session->sess_destroy();
		redirect('login');
	}


	public function viewprofile(){
		$usertype=$this->session->userdata('usertype');
		if($usertype!='Admin'){
			redirect('Login');
		}

		$id=$this->session->userdata('id');


		$data['org']=$this->Common_m->getorg($id);
		$today=date('Y-m-d');
		foreach($data['org'] as $d){
			$expireddate=$d->expired_date;
		}
		if($expireddate<$today){
		
	$this->session->unset_userdata('login');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('email');
		$this->session->sess_destroy();
			$params['active']='0';
			$data=$this->Common_m->deactiveorg($id,$params);
			$this->session->set_flashdata('error', 'Account is expired please contact with administrator to active your account');
			redirect('Login');
		}
		

		$totalloanapplied=$this->Management_m->getloancount();
		$totalviewed=$this->Management_m->getnotificationcount($id);

		$data['count']=$totalloanapplied-$totalviewed;
$data['bod']=$this->Common_m->getbod($id);
	$data['lcm']=$this->Common_m->getlcm($id);
	$data['omt']=$this->Common_m->getomt($id);
	$data['org']=$this->Common_m->getorg($id);
		$this->load->view('header');
		$this->load->view('menu',$data);
		$this->load->view('organization/org_detail',$data);
	}

//active the account for load 
	public function active(){


		$usertype=$this->session->userdata('usertype');
		if($usertype!='SuperAdmin'){
			redirect('Login');
		}
		$email=$this->input->get('email');
		$params['active']="1";
		$data=$this->Common_m->active($email,$params,'cooperative_list');
		if($data){
			$this->session->set_flashdata('success', 'Successfully active your account ');
			redirect('login');

		}

	}



	public  function sentmail(){
		$usertype=$this->session->userdata('usertype');
		if($usertype!='SuperAdmin'){
			redirect('Login');
		}

		$this->load->library('email');
		$params['email']=$this->input->get('email');


//$comfirmpass=$this->input->post('confirm_password');


		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'gator4164.hostgator.com';
		$config['smtp_port']    = '587';
		$config['smtp_timeout'] = 5;
		$config['smtp_user']    = 'admin@cicnp.com';
		$config['smtp_pass']    = 'admin@cic123##';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";

		$config['mailtype'] = 'html'; 
		$config['validation'] = TRUE;

		$this->email->initialize($config);
		$this->email->from('admin@cicnp.com', 'cicnp');
		$this->email->to($params['email']);
		$this->email->subject('Please active your account');

		$contact='<br clear="all" />
		<a href=http://www.cicnp.com/Login/active?email='.$params['email'].'>Click to activate your account<a>';


			$this->email->message($contact);
          //  $this->email->send();


			if(!$this->email->send())
			{
				echo $this->email->print_debugger(); die;
				$this->session->set_flashdata('error', 'Server error please try again!');
				redirect('login/getusersloans');


			}
			else
			{
				$this->session->set_flashdata('success', 'Email sent successfully');
				redirect('login/getusersloans');
			}
		}


		function delete(){
			if(!$this->session->userdata('login')){
				redirect('login');
			}
			$data['id']=$this->input->get('id');
			$data['users']=$this->Common_m->deleteuser($data['id']);

			$this->load->view('header');

			$this->load->view('menu');

			$this->load->view('admin/delete',$data);
		}


		function confirm_delete(){
			$id=$_POST['id'];

			$params=$this->Common_m->getsalt($id);
			foreach($params as $p);{
				unlink ("./uploads/bodphoto/".$p->bod_photo );
			}

			$data=$this->Common_m->delete($id,'cooperative_list');
			$this->session->set_flashdata('success', 'Delete successfully');
			redirect('login/getalluser');	
   	//}
   	 // else{
			$this->session->set_flashdata('error', ' Server error  Please try again !');
			redirect('login/getalluser');	

		}

   	 //add bod member
		function addbod_view(){
			if(! $this->session->userdata('login')){
				redirect('login');
			}

			$data['id']=$this->input->get('id');
			$this->load->view('header');
			$this->load->view('navigation');
			$this->load->view('admin/add_bod',$data);
		}





		function activeorg(){
			$usertype=$this->session->userdata('usertype');
			if($usertype!='SuperAdmin'){
				redirect('Login');
			}
			$id=$this->input->get('org');

			$end=date('Y-m-d', strtotime('+1 year'));
			$params['expired_date']=$end;
			$params['active']='1';
			$data=$this->Common_m->activeorg($id,$params);
			if($data){

				$this->session->set_flashdata('success', 'Organization account has been made active'); 

				redirect('login/getalluser');

			}
		}
		function deactiveorg(){
			$usertype=$this->session->userdata('usertype');
			if($usertype!='SuperAdmin'){
				redirect('Login');
			}
			$id=$this->input->get('org');
			$params['active']='0';
			$data=$this->Common_m->deactiveorg($id,$params);
			if($data){

				$this->session->set_flashdata('success', 'Organization account has been made deactive'); 

				redirect('login/getalluser');

			}
		}

		function changephoto(){
			if(!$this->session->userdata('login')){
				redirect('Login');
			}
			$usertype=$this->session->userdata('usertype');
			if($usertype!='Admin'){
				redirect('Login');
			}

			$id=$this->session->userdata('id');
			


			$totalloanapplied=$this->Management_m->getloancount();
			$totalviewed=$this->Management_m->getnotificationcount($id);

			$data['count']=$totalloanapplied-$totalviewed;


			$this->load->view('header');
			$this->load->view('menu',$data);
			$this->load->view('admin/changephoto');
		}	

		function updatephoto(){
			$usertype=$this->session->userdata('usertype');
			if(!$this->session->userdata('login')){
				redirect('Login');
			}
			if($usertype!='Admin'){
				redirect('Login');
			}
			$id=$this->session->userdata('id');




			$config['upload_path'] = './uploads/organization';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['overwrite']    =  TRUE;

        //$config['encrypt_name'] = true;

			$config['max_size'] = '1000000';
    //          $config['max_width']  = '1024';
        // $config['max_height']  = '720';   
			$config['overwrite']    =  TRUE;
			$config['file_name']  = uniqid(rand());  
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$type= explode('.',$_FILES['image']['name']);
			$type= $type[count($type)-1];

			$params['photo'] = $config['file_name'].'.'.$type;


			$upload = $this->upload->data();


			if(!$this->upload->do_upload('image'))

			{


				$error = array('error' => $this->upload->display_errors());              
				$this->session->set_flashdata('error', $this->upload->display_errors());  
			}
			else{

				$getimage=$this->Common_m->getphoto($id);

				foreach($getimage as $i){
					unlink ("./uploads/organization/".$i->photo );
				}
				$data=$this->Common_m->update($id,$params,'cooperative_list');


				if($data){


					$session_array = array(
						'photo'=>$params['photo']);
					$this->session->set_userdata($session_array);
					$this->session->set_flashdata('success','Successfully photo changed');
					redirect('Login/viewprofile');
				}
				else{
					$this->session->set_flashdata('error','Server error please try again !');
					redirect('Login/viewprofile');
				}

			}

		}

		function searchorg(){
			$name = $this->Common_m->searchvalue_handler($this->input->get_post('name', TRUE));

			$usertype=$this->session->userdata('usertype');

			if(!$this->session->userdata('login')){
				redirect('Login');
			}
			if($usertype!='SuperAdmin'){
				redirect('Login');
			}
			if($this->input->get_post('name')){
				redirect('Login/searchorg');
			}
			$this->load->view('header');
			$this->load->view('navigation');

			$data['sn']=1;
			$data['links']='';

			$data['users']=$this->Common_m->getorgname($name);
			$this->load->view('admin/user',$data);

		}



		function searchloan(){
			$name = $this->Common_m->searchvalue_handler($this->input->get_post('name', TRUE));

			$usertype=$this->session->userdata('usertype');

			if(!$this->session->userdata('login')){
				redirect('Login');
			}
			if($usertype!='SuperAdmin'){
				redirect('Login');
			}
			if($this->input->get_post('name')){
				redirect('Login/searchloan');
			}

			$this->load->view('header');
			$this->load->view('navigation');

			$data['sn']=1;
			$data['links']='';

			$data['users']=$this->Common_m->getloanname($name);
			$this->load->view('loan_user/loan_user',$data);


		}


		public function  searchlist(){
 $id=$this->session->userdata('id');

$searchtype = $this->Management_m->searchterm_handler($this->input->get_post('search', TRUE));
 //==$searchtype=$this->input->post('search');
// $searchvalue=$this->input->post('search_item');
$searchvalue = $this->Management_m->searchvalue_handler($this->input->get_post('search_item', TRUE));
if($this->input->get_post('search') && $this->input->get_post('search_item')){
  redirect('Login/searchlist');
}

 


 if($searchtype=="0"){
   $this->session->set_flashdata('error_select','Please select type');
   redirect('Blacklist');
 }
 else if($searchtype=="1"){
    $data['users']=$this->Black_m->getallblacklistname($searchvalue);


 
   $this->load->view('header');
   $this->load->view('navigation');
   $this->load->view('admin/searchblack',$data);
   

 }
 else{
    $data['users']=$this->Black_m->getblacklistname($searchvalue);
   $this->load->view('header');
   $this->load->view('navigation');
   $this->load->view('admin/searchblack',$data);

 }

}

function forgetpass(){
  $this->load->view('header');

  $this->load->view('admin/email_form');
}

function forgotpassword(){
	$this->load->library('encrypt');
	
$key='Umaharjan123';





	$this->load->library('email');
		$params['email']=$this->input->post('email');

		
		$encrypted_string = $this->encrypt->encode($params['email'],$key);


//$comfirmpass=$this->input->post('confirm_password');


	
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'gator4164.hostgator.com';
    $config['smtp_port']    = '587';
    $config['smtp_timeout'] = 5;
    $config['smtp_user']    = 'admin@cicnp.com';
    $config['smtp_pass']    = 'admin@cic123##';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";

    $config['mailtype'] = 'html'; 
    $config['validation'] = TRUE;

    $this->email->initialize($config);
    $this->email->from('admin@cicnp.com', 'cicnp');
    $this->email->to($params['email']);
    $this->email->subject('Please active your account');

		$contact='<br clear="all" />
		<a href=http://www.cicnp.com/Login/resetpass?email='.$encrypted_string.'>Click to reset password<a>';


			$this->email->message($contact);
          //  $this->email->send();


			if(!$this->email->send())
			{
				
				$this->session->set_flashdata('error', 'Server error please try again!');
				redirect('Login');


			}
			else
			{
				$this->session->set_flashdata('success', 'Email sent successfully');
				redirect('Login');
			}

}

function resetpass(){
	$this->load->view('header');
	$encrypted_string=$this->input->get('email');

		$this->load->library('encrypt');
	
$key='Umaharjan123';

	 $data['email']= $this->encrypt->decode($encrypted_string,$key);
  
   $this->load->view('admin/reset_password',$data);

}

function reset_password(){
	$data['email']=$this->input->post('email');
	$newpass=$this->input->post('new_password');
	$c_password=$this->input->post('c_password');
	if($newpass!=$c_password){
		$this->session->set_flashdata('error', 'Password mismatch');
	$this->load->view('admin/reset_password',$data);	
	}
	else{
	$params['salt']=substr(md5(uniqid(rand(), true)), 0, 6);
	$params['Password']=sha1($newpass.$params['salt'].'Db_Project');
	$update=$this->Common_m->updatepassword($data['email'],$params)	;
	if($update){
		$this->session->set_flashdata('success', 'Password reset successfully');
	redirect('Login');
	}
	}
	}


	function backupfile() {
    $this->load->dbutil();


// Backup your entire database and assign it to a variable
$backup = $this->dbutil->backup();

// Load the file helper and write the file to your server
$this->load->helper('file');
write_file('/uploads/backup/mybackup'.date('Y-m-d').'.sql', $backup); 

// Load the download helper and send the file to your desktop
$this->load->helper('download');
force_download('mybackup'.date('Y-m-d').'.sql', $backup);

  
}



function getsystemblacklist(){

	if(!$this->session->userdata('login')){
		redirect('login');
	}

	$data['users']=$this->Common_m->getsystemblacklist();


	$this->load->view('header');
	$this->load->view('navigation');
	$this->load->view('admin/systemblacklist',$data);
}

	}
	?>
