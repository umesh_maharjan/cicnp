<br>
<H3 style="text-align:center"> Operation Management Team List</H3>
<div class="container">

	<?php 
	if($this->session->flashdata('success')){
		echo '<div class="alert alert-success">';
		echo $this->session->flashdata('success');
		echo '</div>';
	}

	?>
	<?php 
	if($this->session->flashdata('error')){
		echo '<div class="alert alert-danger">';
		echo $this->session->flashdata('error');
		echo '</div>';
	}

	?>

	<div class="row">
		
		<div class="col-md-12" style="text-align:right"> <a href="<?php echo base_url();?>management/addomtview" class="btn btn-success btn-md" ><i class="fa fa-plus"></i>ADD OMT</a></div>
		
	</div>


	<table class=" table table-bordered table-hover table-responsive" style="margin-top:20px;">
		<tr>
			<th>S.N</th>
			<th>Name</th>
			<th>Position</th>
			<th>Action</th>
		</tr>

		
		<?php 

		if(!empty($otm)){
			$sn = 1;
			foreach($otm as $r){ 
				?>
				<tr> 

					<th> <?php echo $sn?>
					</th>

					<th> <?php echo ucwords($r->name);?>
					</th>
					<th> <?php echo ucfirst($r->position);?>
					</th>

					<th style="padding:6px">
						<a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php echo base_url();?>management/editomtview?id=<?php echo $r->id;?>">Edit</i></i></a> 
						<a class="btn btn-danger btn-md"style="padding:8px 20px 8px 20px;" href="<?php echo base_url();?>management/deleteomt?id=<?php echo $r->id;?>">Delete</i></i></a> 
					</th>
					
				</tr>
				<?php  $sn++;} } else {
					?>
					

				
					
				
				</table>
<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
There is no Operation Management Team list</div>
<?php } ?>


			</div>
			<script type="text/javascript">
				document.getElementById('nav_user').className += "active";
			</script>
