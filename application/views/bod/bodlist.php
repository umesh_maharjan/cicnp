
<style>
  #nav_content .dropdown-toggle
{
    background-color:#74AFAD;

  color:white !important;
}
</style>
<br>
<H3 style="text-align:center"> BOD  Member List </H3>
<div class="container">

	<?php 
	if($this->session->flashdata('success')){
		echo '<div class="alert alert-success">';
		echo $this->session->flashdata('success');
		echo '</div>';
	}

	?>
	<?php 
	if($this->session->flashdata('error')){
		echo '<div class="alert alert-danger">';
		echo $this->session->flashdata('error');
		echo '</div>';
	}

	?>

	<div class="row">
		
		<div class="col-md-12" style="text-align:right"> <a href="<?php echo base_url();?>management/addbod_view" class="btn btn-success btn-md" ><i class="fa fa-plus"></i>Add BOD</a></div>
		
	</div>


	<table class=" table table-bordered table-striped table-hover table-responsive" style="margin-top:20px;">
		<tr>
			<th>S.N</th>
			<th>Photo</th>

			<th>Name</th> 
			<th>Address</th>
			<th>Gender</th>
			<th>Contact Number</th>
			
			<th>Education qualification</th>

			<th>Organization</th>

			<th>Action</th>
			
			
			
		</tr>

		
		<?php 

		if(!empty($bod)){
			$sn = 1;
			foreach($bod as $r){ 

				$contactarray = explode(',', $r->contact);
				$eduarray = explode(',', $r->edu_qulfy);?>
			<tr> 

				<th> <?php echo $sn?>
				</th>
				<th> <img src="<?php echo base_url();?>uploads/bodphoto/<?php echo $r->photo;?>" width="80" height="80">
				</th>

				<th> <?php echo ucwords($r->name);?>
				</th>
				<th> <?php echo ucwords($r->address);?>
				</th>
				
				<th> <?php echo $r->gender;?>
				</th>
				<th> 
						<?php foreach($contactarray as $c){
							?>
							<li>
								<?php echo $c;?></li>
								<?php 
							}?>

						</th>
					
						<th> 
							<?php foreach($eduarray as $edu){
								?>
								<li>
									<?php echo $edu;?></li>
									<?php 
								}?>

							</th>



				<th> <?php echo ucwords($r->org_name);?>
							</th>

				<th style="padding:6px">
					<a class="btn btn-success btn-md"style="padding:8px 20px 8px 20px;" href="<?php echo base_url();?>management/editbod_view?id=<?php echo $r->id;?>">Edit</i></i></a> 
					<a class="btn btn-danger btn-md"style="padding:8px 20px 8px 20px;" href="<?php echo base_url();?>management/delete?id=<?php echo $r->id;?>">Delete</i></i></a> 
				</th>
				
			</tr>
			
			<?php  $sn++;} } else {
				?>
					
				
			</table>
			<div  style="font-size:18px; color:red;text-align:center"class="alert alert-success">
There is no  Board of   Director list</div>
<?php } ?>



		</div>

