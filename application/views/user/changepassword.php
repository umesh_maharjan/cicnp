<br>
<style type="text/css">
  


  #nav_setting .dropdown-toggle
{
    background-color:#74AFAD;

  color:white !important;
}
</style>
<div class="container">

<h3 style="text-align:center">Change Password Form</h3>

<form id="edit_user" method="post" action="<?php echo base_url('Management/updatepassword'); ?>" class="form-horizontal">
<?php 

if($this->session->flashdata('error')){
    echo '<div class="alert alert-danger">';
    echo $this->session->flashdata('error');
    echo '</div>';
}
if($this->session->flashdata('success')){
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('success');
    echo '</div>';
}

?>


     <div class="form-group">
    <label for="password" class="col-sm-2 control-label"> Old Password</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" name="old_password" id="old_password" required/>
    </div>
    </div>

     <div class="form-group">
    <label for="password" class="col-sm-2 control-label"> New Password</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" name="new_password" id="new_password" required/>
    </div>

    </div>

    <div class="form-group">
    <label for="confirm_password" class="col-sm-2 control-label">Confirm Password</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" name="confirm_password" id="confirm_password" required/>
    </div>
    </div> 
     <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />


</form>
</div>
<script>
   
    document.getElementById('nav_password').className += "active";
</script>
