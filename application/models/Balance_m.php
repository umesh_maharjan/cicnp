<?php 
class Balance_m extends CI_Model{

	function getorglist(){
	$this->db->select('*')->from('cooperative_list')->where('usertype','Admin');
	return $this->db->get()->result();

	}


	function getinfo($limit,$offset){
		$this->db->select('add_balance.*,cooperative_list.name')->from('add_balance')

		  ->join('cooperative_list','add_balance.org_id=cooperative_list.org_id');
		    $this->db->limit($limit,($offset*$limit));
		       $this->db->order_by('add_balance.created_date','desc');
		return $this->db->get()->result();
	}

	function checkorg($org_id){
		$this->db->select('*')->from('customer')->where('org_id',$org_id);
	return $this->db->get()->result();
	}

	 function insert($input){
  return $this->db->insert('add_balance',$input);
}


public function update($input,$id)

{

  return $this->db->where('id', $id)->update('add_balance', $input);
}


function geteditdata($id){
		$this->db->select('add_balance.*,cooperative_list.name')->from('add_balance')

		  ->join('cooperative_list','add_balance.org_id=cooperative_list.org_id')
		  ->where('add_balance.id',$id);
	return $this->db->get()->result();

}

public function delete($id){
		return
		$this->db->where('id',$id)
		->delete('add_balance');
	}


	function totalorganization(){
  $this->db->select('id')
  ->from('add_balance');
  return $this->db->get()->num_rows();
}


}