<?php 
class Common_m extends CI_Model{

 function insert($input,$tbl){
  return $this->db->insert($tbl, $input);
}

public function update($id,$input,$tbl)

{

  return $this->db->where('org_id', $id)->update($tbl, $input);
}


public function delete($id,$tbl){
  return
  $this->db->where('org_id',$id)
  ->delete($tbl);
}

//org count
function totalcountorg(){
  $this->db->select('org_id')
  ->from('cooperative_list')
  ->where('usertype','Admin');
  return $this->db->get()->num_rows();
}

//loan count
function totalcountloan(){
  $this->db->select('org_id')
  ->from('cooperative_list')
  ->where('usertype','Loan');
  return $this->db->get()->num_rows();
}

function getusers($limit,$offset){
  $this->db->select('org_id,name,active,email')->from('cooperative_list');
  $this->db->order_by('org_id','desc');
  $this->db->limit($limit,($offset*$limit));
  $this->db->where('usertype','Admin');
  return $this->db->get()->result();


}
function getusersloans($limit,$offset){
  $this->db->select('org_id,name,active,email')->from('cooperative_list');
  $this->db->order_by('org_id','desc');
  $this->db->limit($limit,($offset*$limit));
  $this->db->where('usertype','Loan');
  return $this->db->get()->result();


}

public function getusersinfo($email,$password){
 $this->db->select('*')->from('cooperative_list')->where('email',$email)->where('password',$password);
 return $this->db->get()->result();
}

public function getusersalt($email){
  $this->db->select('org_id,salt')->from('cooperative_list')->where('email',$email);
  return $this->db->get()->result();
}


public function checckemail($email){
	$this->db->select('email')->from('cooperative_list')->where('email',$email);
	return $this->db->get()->result();

}
public function checckreg($reg_no){
	$this->db->select('reg_no')->from('cooperative_list')->where('reg_no',$reg_no);
	return $this->db->get()->result();

}
//get all information about organization
public function getorg($id){
  $this->db->select('*')->from('cooperative_list')->where('org_id',$id);
  return $this->db->get()->result();
}



public function getbod($id){
  $this->db->select('*')->from('member')
  ->where('organization_id',$id)
  ->where('type','BOD');
  $this->db->order_by('id','desc');
  return $this->db->get()->result(); 
}
public function getlcm($id){
  $this->db->select('*')->from('member')
  ->where('organization_id',$id)
  ->where('type','LCM');
  $this->db->order_by('id','desc');
  return $this->db->get()->result(); 
}

public function getomt($id){
  $this->db->select('*')->from('otm')
  ->where('org_id',$id);
  $this->db->order_by('id','desc');


  return $this->db->get()->result(); 
}



function getblacklist(){
  $this->db->select('*')->from('user_detail');


  $this->db->order_by('black_id','desc');

  return $this->db->get()->result(); 
}

function getsystemblacklist(){
  $this->db->select('user_detail.*,cooperative_list.name as companyname')->from('user_detail');
  $this->db->join('cooperative_list','cooperative_list.org_id=user_detail.organization_id');


  $this->db->order_by('black_id','desc');

  return $this->db->get()->result(); 
}




 //get all information about loaner
public function getloan_info($id){
  $this->db->select('cooperative_list.*,loan_user_details.Gender,loan_user_details.DOB,loan_user_details.Education,loan_user_details.Occupation')
  ->from('cooperative_list')
  ->join('loan_user_details','cooperative_list.org_id=loan_user_details.user_id')
  ->where('cooperative_list.org_id',$id);
  return $this->db->get()->result();
}

public function checkdublicatedemail($email){
  $this->db->select('email')->from('cooperative_list')->where('email=',$email);
  return $this->db->get()->result();

}

public function getsalt($id){
 $this->db->select('org_id,salt,password,email')->from('cooperative_list')->where('org_id',$id);
 return $this->db->get()->result();

}

public function active($id,$input,$tbl)

{

  return $this->db->where('email', $id)->update($tbl, $input);
}

public function activeorg($id,$input)

{

  return $this->db->where('org_id', $id)->update('cooperative_list', $input);
}

public function deactiveorg($id,$input)

{

  return $this->db->where('org_id', $id)->update('cooperative_list', $input);
}



public function deleteuser($id){
  $this->db->select('name')->from('cooperative_list')->where('org_id',$id);
  return $this->db->get()->result();
}



public function updatepass($email,$input){

  return $this->db->where('email', $email)->update('cooperative_list', $input); 
}
public function getphoto($id){
  $this->db->select('photo')->from('cooperative_list')->where('org_id',$id);
  return $this->db->get()->result();
}

public function getorgname($name){

  $where="name LIKE '%" .$name."%'";
  $this->db->select('*')->from('cooperative_list');
  $this->db->order_by('org_id','desc');

  $this->db->where('usertype','Admin');
  $this->db->where($where);
  return $this->db->get()->result();
}

public function searchvalue_handler($searchterm)
{
  if($searchterm)
  {
    $this->session->set_userdata('search_name', $searchterm);
    return $searchterm;
  }
  elseif($this->session->userdata('search_name'))
  {
    $searchterm = $this->session->userdata('search_name');
    return $searchterm;
  }
  else
  {
    $searchterm ="";
    return $searchterm;
  }
}

public function getloanname($name){

  $where="name LIKE '" .$name."%'";
  $this->db->select('*')->from('cooperative_list');
  $this->db->order_by('org_id','desc');
  
  $this->db->where('usertype','Loan');
  $this->db->where($where);
  return $this->db->get()->result();
}

public function updatepassword($email,$input)

{

  return $this->db->where('email', $email)->update('cooperative_list', $input);
}
}

?>