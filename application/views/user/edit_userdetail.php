<br>

<?php foreach($black as $r){
    $name=$r->name;
    $f_name=$r->father_name;
    $g_name=$r->grandfather_name;
    $p_address=$r->permanent_address;
    $contact=$r->contact_no;
    $citizen_no=$r->citizen_number;
    $upload_by=$r->upload_by;
    $citizen_address=$r->citizen_issued_place;
    $bod=$r->bod;
    $image=$r->photo;
    $organization_name=$r->org_name;
    $id=$r->black_id;


}

$myArray = explode(',', $contact);?>

<div class="container">
    <h3 style="text-align:center">Edit Blacklist Form</h3>

    <form id="add_user" method="post" action="<?php echo base_url('blacklist/editblacklist'); ?>" class="form-horizontal"  enctype="multipart/form-data">

        <?php 

        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('success');
            echo '</div>';
        }
        if($this->session->flashdata('error')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('error');
            echo '</div>';
        }
        ?>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="name" id="name" value="<?php echo $name;?>" style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Father Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="f_name" id="f_name"  style="text-transform: capitalize;" value="<?php echo $f_name;?>" required/>
            </div>  
        </div>
        <div class="form-group">

            <label for="firstname" class="col-sm-2 control-label"> Grandfather Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="g_name" id="g_name" value="<?php echo $g_name;?>"style="text-transform: capitalize;" required/>
            </div>  
        </div>

        <div class="form-group">
            <label  class="col-sm-2 control-label"> Permanent Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="p_address" style="text-transform: capitalize;" value="<?php echo $p_address;?>" id="address" required />
            </div>
        </div>

  <div class="form-group">
            <label for="contact" class="col-sm-2 control-label"> Contact Number</label>
            <div class="col-sm-7">
                <?php 
                $ca=0;
                foreach($myArray as $a) {
                 $ca=1;
                 

                 echo "<div id='editcontact".$ca."'><input type='text' pattern='[0-9]{7,10}'' class='form-control' value=".$a." name='contact[]' id='contactbod".$ca."'  placeholder='Contact' required/><div class='col-sm-0'><button class='btn btn-default'  onclick='remove_editcont(".$ca.")'>Remove</button></div></div>";
                 $ca=$ca+1;


             }?>
         </div>
         <div class="col-sm-0">
            <button class="btn btn-default" id="add_more_contact" onclick="addContact()">Add More</button>
        </div>
    </div> 


    <div id="add_contact"></div> 





        <div class="form-group">
            <label for="lastname" class="col-sm-2 control-label">Cititzen Number</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" name="citizen_no" id="citizen_no"  value="<?php echo $citizen_no;?>"required/>
            </div>
        </div>
        


        <div class="form-group">
            <label  class="col-sm-2 control-label">Citizen Issued Place</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="citizen_place"  value="<?php echo $citizen_address;?>" id="citizen_place" required />
            </div>
        </div>
        
        
        <div class="form-group">
            <label  class="col-sm-2 control-label">Birth of date</label>
            <div class="col-sm-8">
                <input type="date" class="form-control" name="bod" value="<?php echo $bod;?>" id="bod" required />
            </div>
        </div>
        
        <div class="form-group">
            <label  class="col-sm-2 control-label">Photo</label>
            <div class="col-sm-8">
                <input type="file" class="form-control" name="image"  id="image"  />

            </div>
        </div>
        <div class="form-group">
         <label  class="col-sm-2 control-label"></label>
         <div class="col-sm-2">
            <img src="<?php  echo base_url();?>uploads/black/<?php echo $image;?>" width="100" height="100">
            
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-2 control-label">Upload by </label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="upload" value="<?php echo $upload_by;?>" id="upload" required />
        </div>
    </div>




    <input type="hidden" name="id" id="id" value="<?php echo $id?>">

    
    <input type="submit" name="submit" id="submit" value="Save" class="btn btn-success" style="margin-left:43%;" />
</form>
</div>
<script type="text/javascript">
    var count= 0;
    var addContact = function(){
        count = count+1;
        console.log(count);
        $("#add_contact").append("<div class='container' id='contact"+count+"'> <div class='form-group'><div class='col-sm-2'></div><div class='col-sm-6'><input type='text' pattern='[0-9]{7,10}' class='form-control' name='contact[]'  style='text-transform: capitalize;' placeholder='Contact' required/></div><div class='col-sm-0'><button class='btn btn-default' id='contact"+count+"' onclick='removeContact("+count+")'>Remove</button></div></div>");
    };

    var removeContact = function(c){
        console.log("remove count: "+c);
        console.log("contact"+c);
        $("#contact"+c).remove();
        
    };



    var remove_editcont= function(ed){
        // console.log("remove countedu: "+ed);
        // console.log("education"+ed);
        $("#editcontact"+ed).remove();
        
    };



</script>

